export interface ISearchList {
    id: string;
    key: string;
    type: string;
    keyword: string;
    count: number;
    createAt: string;
    updateAt: string;
}

export interface ISearchParams {
    page: number;
    pageSize: number;
    type: string;
    keyword: string;
    count: string;
}