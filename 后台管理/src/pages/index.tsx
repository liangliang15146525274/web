import React, { ChangeEvent, useEffect, useRef, useState } from 'react'
import { Card, Col, Popover, Row, Empty, Popconfirm, Modal, Input, ConfigProvider } from 'antd';
import { NavLink } from 'react-router-dom'
import * as echarts from 'echarts';
import styles from './index.less'
import useStore from '@/context/useStore';
import { observer } from 'mobx-react-lite'
import classNames from 'classnames';
import zhCN from 'antd/es/locale/zh_CN';

const { TextArea } = Input;

const Home: React.FC = (props) => {
    const store = useStore();
    const commentList = store.comment.commentList
    const [isModalVisible, setIsModalVisible] = useState(false);
    const [val, setVal] = useState('');

    const eChartsRef: any = useRef(null);
    useEffect(() => {
        const chart = echarts.init(eChartsRef.current);
        let option: any = {
            tooltip: {
                trigger: 'axis',
                axisPointer: {
                    type: 'cross',
                    crossStyle: {
                        color: '#999'
                    }
                }
            },

            legend: {
                data: ['评论数', '访问量']
            },
            xAxis: [
                {
                    type: 'category',
                    data: ['Mon', 'Tue', 'Web', 'Thu', 'Fri', 'Sta', 'Sun'],
                    axisPointer: {
                        type: 'shadow'
                    }
                }
            ],
            yAxis: [
                {
                    type: 'value',
                    name: '每周客户访问指标',
                    min: 0,
                    max: 400,
                    interval: 100,
                    axisLabel: {
                        formatter: '{value}',
                        fontWeight: 800
                    }
                },
                {
                    type: 'value',
                    min: 0,
                    max: 400,
                    interval: 100,
                },

            ],
            series: [
                {
                    name: '评论数',
                    type: 'bar',
                    data: [110, 200, 150, 90, 80, 105, 120],
                    color: '#C23531'
                },
                {
                    name: '访问量',
                    type: 'line',
                    yAxisIndex: 1,
                    data: [380, 295, 340, 110, 100, 120, 160]
                }
            ]
        };

        chart.setOption(option);
    }, [])

    useEffect(() => {
        store.comment.commentData({ page: 1, pageSize: 6 })
        store.comment.getarticleWorkList({ page: 1, pageSize: 6 })
    }, [])

    //每个删除按钮
    const confirmEvery = (id: string) => {
        console.log(id);
        store.comment.delComment([id], 1, 6)
    }

    //回复框
    const showModal = () => {
        setIsModalVisible(true);
    };

    const handleOk = () => {
        setIsModalVisible(false);
    };

    const handleCancel = () => {
        setIsModalVisible(false);
    };

    //改变评论内容
    function changeValue(val: string) {
        setVal(val)
    }
    return (
        <div className={classNames(styles.viewBox, 'workBox')}>
            <Card title="面板导航" bordered={false}>
                <div ref={eChartsRef} className='mainBox'></div>
            </Card>
            <Card title="快速导航" bordered={false} style={{ marginTop: '24px' }}>
                <Row gutter={16}>
                    <Col span={4} style={{ padding: '2rem 1rem', textAlign: 'center' }}>
                        <NavLink to='/article' className={styles.a}><span>文章管理</span></NavLink>
                    </Col>
                    <Col span={4} style={{ padding: '2rem 1rem', textAlign: 'center' }}>
                        <NavLink to='/comment' className={styles.a}><span>评论管理</span></NavLink>
                    </Col>
                    <Col span={4} style={{ padding: '2rem 1rem', textAlign: 'center' }}>
                        <NavLink to='/file' className={styles.a}><span>文件管理</span></NavLink>
                    </Col>
                    <Col span={4} style={{ padding: '2rem 1rem', textAlign: 'center' }}>
                        <NavLink to='/user' className={styles.a}><span>用户管理</span></NavLink>
                    </Col>
                    <Col span={4} style={{ padding: '2rem 1rem', textAlign: 'center' }}>
                        <NavLink to='/view' className={styles.a}><span>访问管理</span></NavLink>
                    </Col>
                    <Col span={4} style={{ padding: '2rem 1rem', textAlign: 'center' }}>
                        <NavLink to='/setting' className={styles.a}><span>系统设置</span></NavLink>
                    </Col>
                </Row>
            </Card>
            <Card title="最新文章" bordered={false} style={{ marginTop: '24px' }} extra={<NavLink to='/article' className={styles.a}><span>全部文章</span></NavLink>}>
                {
                    store.comment.articleWorkList && store.comment.articleWorkList.map((item, index) => {
                        return <a href={`/article/editor/${item.id}`} key={item.id} style={{ color: '#000' }}>
                            <Card.Grid style={{ width: '33.3%', textAlign: 'center' }}>
                                <img src={item.cover} alt="文章封面" style={{ display: 'inline-block', width: '100%', height: '110px' }} />
                                <p>{item.title}</p>
                            </Card.Grid>
                        </a>
                    })
                }
            </Card>
            <Card title="最新评论" bordered={false} style={{ marginTop: '24px' }} extra={<NavLink to='/comment' className={styles.a}><span>全部评论</span></NavLink>}>
                <ul className={styles.ulsBox} >
                    {
                        commentList ? commentList.map((item, index) => {
                            let content = item.url
                            let content2 = item.content
                            return <ConfigProvider locale={zhCN} key={index}>
                                <li>
                                    <div>
                                        <span style={{ marginRight: '5px' }}>{item.name}</span>
                                        {'在'}
                                        <Popover placement="right" content={<iframe src={'https://creation.shbwyz.com' + content} style={{ width: '500px', height: '500px' }}></iframe>} title="页面预览" trigger="hover">
                                            <a href={`https://creationadmin.shbwyz.com/creationad.shbwyz.com/article/${item.hostId}`} style={{ margin: '0 5px' }}>{'文章'}</a>
                                        </Popover>
                                        {'评论'}
                                        <Popover content={content2} title="评论详情-原始内容" trigger="hover">
                                            <span style={{ color: '#0188fb', cursor: 'pointer', margin: '0 5px' }}>{'查看评论'}</span>
                                        </Popover>
                                        {
                                            item.pass ? <span className={styles.badge} style={{ marginLeft: '10px' }}>
                                                <span className={styles.antdGold}></span> <span>{'通过'}</span>
                                            </span> :
                                                <span className={styles.badge} style={{ marginLeft: '10px' }}>
                                                    <span className={styles.antdGreen}></span> <span>{'未通过'}</span>
                                                </span>
                                        }
                                    </div>
                                    <div className={styles.rightBox}>
                                        <a onClick={() => store.comment.updateComment([item.id], { pass: true }, 1, 6)}>通过</a><span>|</span>
                                        <a onClick={() => store.comment.updateComment([item.id], { pass: false }, 1, 6)}>拒绝</a><span>|</span>
                                        <a type="primary" onClick={showModal}>回复</a>
                                        <Modal title="回复评论" visible={isModalVisible} onOk={handleOk} onCancel={handleCancel} maskStyle={{ backgroundColor: "rgbA(0,0,0,0.05)" }}>
                                            <TextArea
                                                value={val}
                                                onInput={(e: ChangeEvent<HTMLTextAreaElement>) => changeValue(e.target.value)}
                                                placeholder="支持 Markdown"
                                                style={{ height: '142px', minHeight: '142px', maxHeight: '230px', overflowY: 'hidden', resize: 'none' }}
                                            />
                                        </Modal>
                                        <span>|</span>
                                        <Popconfirm
                                            title="确认删除?"
                                            onConfirm={(e?: React.MouseEvent<HTMLElement, MouseEvent>) => confirmEvery(item.id)}
                                            okText="确认"
                                            cancelText="取消"
                                        >
                                            <a>删除</a>
                                        </Popconfirm>

                                    </div>
                                </li>
                            </ConfigProvider>
                        }) : <Empty />
                    }
                </ul>

            </Card>
        </div>
    )
}
export default observer(Home);
