import React from 'react';
import styles from './index.less';
import { InboxOutlined } from '@ant-design/icons';
import { observer } from 'mobx-react-lite';
import { Upload, message, Form, Row, Col, Button, Input } from 'antd';
interface IProp {
  name: string;
  multiple: boolean;
  action: string;
  onChange(info: any): void;
  onDrop(e: any): void;
}
const Poster: React.FC = (props) => {
  const [form] = Form.useForm();
  const prop: IProp = {
    name: 'file',
    multiple: true,
    action: 'https://www.mocky.io/v2/5cc8019d300000980a055e76',
    onChange(info) {
      const { status } = info.file;
      if (status !== 'uploading') {
        console.log(info.file, info.fileList);
      }
      if (status === 'done') {
        message.success(`${info.file.name} file uploaded successfully.`);
      } else if (status === 'error') {
        message.error(`${info.file.name} file upload failed.`);
      }
    },
    onDrop(e) {
      console.log('Dropped files', e.dataTransfer.files);
    },
  };
  const getFields = () => {
    const children = [];
    children.push(
      <Col span={8} key={1} style={{ display: 'flex'}}>
        <Form.Item name="title" label={`文件名称`}>
          <Input placeholder="请输入知识库名称" style={{ width: 185 }} />
        </Form.Item>
      </Col>,
    );
    return children;
  };
  const { Dragger } = Upload;
  const onFinish = (values: any) => {
    console.log(values);
  };
  return (
    <div className={styles.container}>
      <div className={styles.top}>
        <Dragger {...prop} style={{ background: '#fff', width: '100%' }}>
          <p className="ant-upload-drag-icon">
            <InboxOutlined />
          </p>

          <p className="ant-upload-text">点击选择文件或将文件拖拽到此处</p>
          <p className="ant-upload-hint">文件将上传到 OSS, 如未配置请先配置</p>
        </Dragger>
      </div>
      <div className={styles.bottom}>
        <Form
          style={{ padding: '24px 12px' }}
          form={form}
          name="advanced_search"
          className="ant-advanced-search-form"
          onFinish={onFinish}
        >
          <Row gutter={24}>{getFields()}</Row>
          <Row>
            <Col span={24} style={{ textAlign: 'right' }}>
              <Button type="primary" htmlType="submit">
                搜索
              </Button>
              <Button
                style={{ marginLeft: '8px' }}
                onClick={() => {
                  form.resetFields();
                }}
              >
                重置
              </Button>
            </Col>
          </Row>
        </Form>
      </div>
    </div>
  );
};
export default observer(Poster);
