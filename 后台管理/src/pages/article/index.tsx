import React, { useEffect, useState } from 'react';
import { Form, Row, Col, Input, Button, Select, Table, Radio, Divider, Badge, Space, ConfigProvider, Popconfirm, message, Tag } from 'antd';
import { SyncOutlined } from '@ant-design/icons';
import zhCN from 'antd/es/locale/zh_CN';
import { observer } from 'mobx-react-lite';
import './index.less'
import useStore from '@/context/useStore';
import moment from 'moment';
import { useHistory } from 'umi';
import share from '@/components/share';
import { IArticleItem } from '@/types/module/article';
import ConfimArticle from '@/components/ConfimArticle';
const { Option } = Select;


const AdvancedSearchForm = () => {
    const [form] = Form.useForm();
    const history = useHistory()
    const [page, setPage] = useState(1)
    const [pageSize, setPageSize] = useState(12)
    const store = useStore();
    const [show, setShow] = React.useState(false);
    const [selectArry, setSelectArry] = React.useState<React.Key[]>([]);

    // 下线
    const OffLine = async (record: IArticleItem) => {
        if (JSON.parse(localStorage.user).role === 'visitor') {
            message.warning('访客无权进行该操作');
        } else {
            if (record.isRecommended) {
                let result = await store.article.getOutArticle({ id: record.id, isRecommended: false });
                if (result.statusCode === 200) {
                    message.success('操作成功');
                    store.article.getArticleList({ page, pageSize })
                }
            } else {
                let result = await store.article.getOutArticle({ id: record.id, isRecommended: true });
                if (result.statusCode === 200) {
                    message.success('操作成功');
                    store.article.getArticleList({ page, pageSize })
                }
            }
        }
    }

    // 下线
    const OffLines = async (array: React.Key[], status: string) => {
        if (JSON.parse(localStorage.user).role === 'visitor') {
            message.warning('访客无权进行该操作');
        } else {
            await store.article.getOutArticles(array, status);
        }
    }
    // 焦点
    const OffUp = async (array: React.Key[], isRecommended: boolean) => {
        if (JSON.parse(localStorage.user).role === 'visitor') {
            message.warning('访客无权进行该操作');
        } else {
            await store.article.getUpList(array, isRecommended!);
        }
    }

    // 表头
    const columns: any = [
        {
            title: '标题',
            dataIndex: 'title',
            width: 200,
            fixed: 'left',
        },
        {
            title: '状态',
            dataIndex: 'status',
            render: (text: string) => {
                return text === 'publish' ? <Badge status="success" text="已发布" /> : <Badge status="warning" text="草稿" />
            }
        },  
        {
            title: '分类',
            dataIndex: 'category',
            render: (text: any) => {
                return <span>{text && <Tag color={'#' + Math.random().toString(16).substr(2, 6).toUpperCase()}>{text.label}</Tag>}</span>
            }
        },
        {
            title: '标签',
            dataIndex: 'tags',
            width: 200,
            render: (text: {
                id: string;
                label: string;
                value: string;
                createAt: string;
                updateAt: string;
            }[]) => {
                return <span>{text.length > 0 ? text.map(item => <Tag key={item.id} style={{ marginRight: '5px' }} color={'#' + Math.random().toString(16).substr(2, 6).toUpperCase()}>{item.label}</Tag>) : null}</span>
            }
        },
        {
            title: '阅读量',
            dataIndex: 'views',
            render: (text: number) => {
                return <Space onClick={() => console.log(text)}>
                    <Badge
                        className="site-badge-count-109"
                        showZero={true}
                        count={text}
                        style={{ backgroundColor: '#52c41a' }}
                    />
                </Space>
            }
        },
        {
            title: '喜欢数',
            dataIndex: 'likes',
            width: 100,
            render: (text: number) => {
                return <Space onClick={() => console.log(text)}>
                    <Badge
                        className="site-badge-count-109"
                        showZero={true}
                        count={text}
                        style={{ backgroundColor: 'rgb(235, 47, 150)' }}
                    />
                </Space>
            }

        },
        {
            title: '发布时间',
            dataIndex: 'createAt',
            render: (text: string) => {
                return <span>{moment(text).format('YYYY-MM-DD HH:mm:ss')}</span>
            }
        },
        {
            title: '操作',
            dataIndex: 'address',
            width: 320,
            fixed: 'right',
            render: (text: string, record: IArticleItem) => {
                return <div style={{ display: 'flex', flexWrap: 'nowrap', alignItems: 'center' }}>
                    <a><Button type="link" onClick={() => history.push('/article/editor/' + record.id)}>编辑</Button></a><Divider style={{ margin: 0 }} type="vertical" />
                    <a><Button type="link" onClick={() => OffLine(record)}>{record.isRecommended ? '撤销首焦' : '首焦推荐'}</Button></a><Divider style={{ margin: 0 }} type="vertical" />
                    <a><Button type="link" onClick={() => share()}>查看访问</Button></a><Divider style={{ margin: 0 }} type="vertical" />
                    <ConfimArticle title="确认删除？" id={record.id} page={page} pageSize={pageSize}><a><Button type="link">删除</Button></a></ConfimArticle>
                </div>
            },
        },
    ];
    // 文章数据
    useEffect(() => {
        store.article.getArticleList({ page, pageSize })
    }, [])
    // 分类数据
    useEffect(() => {
        store.article.getCategoryList()
    }, [])

    // 搜索
    const onFinish = (values: any) => {
        store.article.getArticleList({ ...{ page, pageSize }, ...values })
    };
    // 切换每页条目数
    function onChange(page: number, pageSize?: number) {
        store.article.getArticleList({ page, pageSize })
    }
    // 新建页面
    function newArticle() {
        history.push('/article/editor')
    }
    // 刷新
    function refresh() {
        store.article.getArticleList({ page, pageSize })
    }
    // 多选
    const rowSelection = {
        onChange: (selectedRowKeys: React.Key[]) => {
            selectedRowKeys.length ? setShow(true) : setShow(false);
            setSelectArry(selectedRowKeys)
        },
    };
    const getFields = () => {
        const children = [];
        children.push(
            <Col span={8} key={1}>
                <Form.Item
                    name={`title`}
                    label={`标题`}
                >
                    <Input placeholder="请输入页面名称" />
                </Form.Item>
            </Col>,
            <Col span={8} key={2}>
                <Form.Item
                    name={`status`}
                    label={`状态`}
                >
                    <Select >
                        <Option value="publish">已发布</Option>
                        <Option value="draft">草稿</Option>
                    </Select>
                </Form.Item>
            </Col>,
            <Col span={8} key={3}>
                <Form.Item
                    name="category"
                    label="分类"
                >
                    <Select >
                        {store.article.categoryList.map(item => {
                            return <Option key={item.id} value={item.id}>{item.label}</Option>
                        })}
                    </Select>
                </Form.Item>
            </Col>
        );
        return children;
    };

    return (
        <div>
            <Form
                form={form}
                name="advanced_search"
                className="ant-advanced-search-form from_top"
                onFinish={onFinish}
            >
                <Row gutter={24} className='article_row'>{getFields()}</Row>
                <Row>
                    <Col span={24} style={{ textAlign: 'right' }}>
                        <Button type="primary" htmlType="submit">
                            搜索
                        </Button>
                        <Button
                            style={{ margin: '0 8px' }}
                            onClick={() => {
                                form.resetFields();
                            }}
                        >
                            重置
                        </Button>
                    </Col>
                </Row>
            </Form>
            <div className='table'>
                <div>
                    <div style={{ opacity: show ? '1' : '0' }}>
                        <Button onClick={() => OffLines(selectArry, 'publish')}>发布</Button>&ensp;
                        <Button onClick={() => OffLines(selectArry, 'draft')}>草稿</Button>&ensp;
                        <Button onClick={() => OffUp(selectArry, true)}>首焦推荐</Button>&ensp;
                        <Button onClick={() => OffUp(selectArry, false)}>撤销首焦</Button>&ensp;
                        <ConfimArticle title="确认删除？" selectedRow={selectArry} page={page} pageSize={pageSize}><a><Button danger>删除</Button></a></ConfimArticle></div>
                    <div>
                        <Button type="primary" onClick={() => newArticle()}>+&nbsp;新建</Button>
                        <span onClick={() => refresh()}><SyncOutlined style={{ width: '16px', marginLeft: '12px' }} /></span>
                    </div>
                </div>
                <ConfigProvider locale={zhCN}>
                    <Table
                        rowSelection={{
                            ...rowSelection,
                        }}
                        rowKey={'id'}
                        columns={columns}
                        dataSource={store.article.articleList}
                        pagination={{
                            defaultPageSize: 12,
                            total: store.article.articleCount,
                            pageSizeOptions: ['8', '12', '24', '36'],
                            defaultCurrent: page,
                            showSizeChanger: true,
                            showTotal: (total => `共 ${store.article.articleCount} 条`),
                            onChange: onChange
                        }}
                        scroll={{ x: 1300 }}
                    />
                </ConfigProvider>
            </div>
        </div>
    );
};
export default observer(AdvancedSearchForm)
