import React, { ChangeEvent, Key, useEffect, useState } from 'react'
import { Form, Row, Col, Input, Button, Select, Table, Badge, Popover, Popconfirm, ConfigProvider, Modal } from 'antd';
import styles from './index.less'
import useStore from '@/context/useStore';
import { observer } from 'mobx-react-lite'
import { commentItem } from '@/types';
import { useHistory } from 'umi';
import zhCN from 'antd/es/locale/zh_CN';
import { fomatTime } from '@/utils/fomatTime';

const { Option } = Select;
const { TextArea } = Input;

const Comment: React.FC = (props) => {
    const [form] = Form.useForm();
    const [page, setpage] = useState(1)
    const [pageSize, setpageSize] = useState(12)
    const store = useStore();
    const history = useHistory();
    const [selectedRowKeys, setSelectedRowKeys] = useState<Key[]>([]);
    const [isModalVisible, setIsModalVisible] = useState(false);
    const [val, setVal] = useState('')

    const onGenderChange = (value: string) => {
        switch (value) {
            case '已通过':
                form.setFieldsValue({ note: '已通过' });
                return;
            case '未通过':
                form.setFieldsValue({ note: '未通过' });
                return;
        }
    };
    const onReset = () => {
        form.resetFields();
        store.comment.commentData({ page: page, pageSize: pageSize })
    };

    //改变多选框状态
    const onSelectChange = (selectedRowKeys: Key[]) => {
        setSelectedRowKeys(selectedRowKeys)
    };
    const rowSelection = {
        selectedRowKeys,
        onChange: onSelectChange,
    };

    //发起评论请求
    useEffect(() => {
        store.comment.commentData({ page: page, pageSize: pageSize })
    }, [page, pageSize])

    //表格头部
    const columns = [{
        title: '状态',
        width: 150,
        fixed: 'left',
        render: (row: commentItem) => {
            return row.pass ? <Badge status="success" text="通过" /> : <Badge status="warning" text="未通过" />
        }
    }, {
        title: '称呼',
        dataIndex: 'name'
    }, {
        title: '联系方式',
        dataIndex: 'email'
    }, {
        title: '原始内容',
        dataIndex: 'content',
        render: (row: commentItem) => {
            return <Popover content={row} title="评论详情-原始内容" trigger="hover">
                <span style={{ color: '#0188fb', cursor: 'pointer', margin: '0 5px' }}>{'查看评论'}</span>
            </Popover>
        }
    }, {
        title: 'HTML内容',
        dataIndex: 'html',
        render: (row: string) => {
            return <Popover content={<div dangerouslySetInnerHTML={{ __html: row }}></div>} title="评论详情-HTML内容" trigger="hover">
                <span style={{ color: '#0188fb', cursor: 'pointer', margin: '0 5px' }}>{'查看内容'}</span>
            </Popover>
        }
    }, {
        title: '管理文章',
        dataIndex: 'url',
        render: (dataIndex: string) => {
            return <Popover content={<iframe src={'https://creation.shbwyz.com' + dataIndex}></iframe>} title="页面预览" trigger="hover">
                <span style={{ color: '#0188fb', cursor: 'pointer', margin: '0 5px' }}>{'查看内容'}</span>
            </Popover>
        }
    }, {
        title: '创建时间',
        dataIndex: 'createAt',
        render: (dataIndex: string) => {
            return fomatTime(dataIndex)
        }
    }, {
        title: '父级评论',
        dataIndex: 'parentcontent',
        render: (dataIndex: string | null) => {
            return <div>
                {
                    dataIndex ? <p>{dataIndex}</p>
                        : <p>{'无'}</p>
                }
            </div >
        }
    }, {
        title: '操作',
        width: 300,
        fixed: 'right',
        render: (row: commentItem) => {
            return <p>
                <Button style={{ border: 'none', color: '#1890ff', background: 'none' }} onClick={() => store.comment.updateComment([row.id], { pass: true }, page, 12)}>通过</Button>
                <Button style={{ border: 'none', color: '#1890ff', background: 'none' }} onClick={() => store.comment.updateComment([row.id], { pass: false }, page, 12)}>拒绝</Button>
                <Button style={{ border: 'none', color: '#1890ff', background: 'none' }} type="primary" onClick={showModal}>回复</Button>
                <Modal title="回复评论" visible={isModalVisible} onOk={handleOk} onCancel={handleCancel} maskStyle={{ backgroundColor: "rgbA(0,0,0,0.05)" }}>
                    <TextArea
                        value={val}
                        onInput={(e: ChangeEvent<HTMLTextAreaElement>) => changeValue(e.target.value)}
                        placeholder="支持 Markdown"
                        style={{ height: '142px', minHeight: '142px', maxHeight: '230px', overflowY: 'hidden', resize: 'none' }}
                    />
                </Modal>
                <Popconfirm
                    title="确认删除?"
                    onConfirm={(e?: React.MouseEvent<HTMLElement, MouseEvent>) => confirmEvery(row.id)}
                    okText="确认"
                    cancelText="取消"
                >
                    <Button style={{ border: 'none', color: '#1890ff', background: 'none' }}>删除</Button>
                </Popconfirm>
            </p>
        }
    }]

    //模糊搜索
    const submit = () => {
        let values = form.getFieldsValue();
        console.log(values);
        if (values.pass === '未通过') {
            values.pass = '0'
        } else if (values.pass === '已通过') {
            values.pass = '1'
        }
        console.log(values);
        store.comment.searchComment({ page: page, pageSize: pageSize }, { name: values.name, email: values.email, pass: values.pass })
    }


    //上部删除按钮
    const confirm = () => {
        store.comment.delComment(selectedRowKeys as string[], page, pageSize)
    }
    //每个删除按钮
    const confirmEvery = (id: string) => {
        console.log(id);
        store.comment.delComment([id], page, 12)
    }

    //切换页码数
    function changePage(pagination: any) {
        setpage(pagination.current);
        setpageSize(pagination.pageSize);
    }

    //回复框
    const showModal = () => {
        setIsModalVisible(true);
    };

    const handleOk = () => {
        setIsModalVisible(false);
    };

    const handleCancel = () => {
        setIsModalVisible(false);
    };

    //改变评论内容
    function changeValue(val: string) {
        setVal(val)
    }
    return (
        <div>
            <Form
                form={form}
                name="advanced_search"
                className="ant-advanced-search-form"
                style={{ background: '#fff' }}
                onFinish={submit}
            >
                <Row gutter={16}>
                    <Col span={5}>
                        <Form.Item
                            name={'name'}
                            label={'称呼'}
                        >
                            <Input placeholder="请输入称呼" />
                        </Form.Item>
                    </Col>
                    <Col span={5}>
                        <Form.Item
                            name={'email'}
                            label={'Email'}
                        >
                            <Input placeholder="请输入联系方式" />
                        </Form.Item>
                    </Col>
                    <Col span={7}>
                        <Form.Item name="pass" label="状态">
                            <Select
                                onChange={onGenderChange}
                                allowClear
                            >
                                <Option value="已通过">已通过</Option>
                                <Option value="未通过">未通过</Option>
                            </Select>
                        </Form.Item>
                    </Col>
                </Row>
                <Row gutter={16}>
                    <Col span={24} style={{ textAlign: 'right' }}>
                        <Form.Item>
                            <Button type="primary" htmlType="submit">
                                搜索
                            </Button>
                            <Button htmlType="button" onClick={onReset}>
                                重置
                            </Button>
                        </Form.Item>
                    </Col>
                </Row>
            </Form>
            <div style={{ background: 'rgb(255, 255, 255)', padding: '24px 12px' }}>
                <div className={styles.sxBox}>
                    <div>
                        {
                            selectedRowKeys.length > 0 ?
                                <div>
                                    <Button style={{ marginRight: '10px' }} onClick={() => store.comment.updateComment(selectedRowKeys as string[], { pass: true }, page, pageSize)}>通过</Button>
                                    <Button style={{ marginRight: '10px' }} onClick={() => store.comment.updateComment(selectedRowKeys as string[], { pass: false }, page, pageSize)}>拒绝</Button>
                                    <Popconfirm
                                        title="确认删除?"
                                        onConfirm={confirm}
                                        okText="确认"
                                        cancelText="取消"
                                    >
                                        <Button danger>删除</Button>
                                    </Popconfirm>
                                </div>
                                : null
                        }

                    </div>
                    <div onClick={() => store.comment.commentData({ page: 1, pageSize: 12 })}>
                        <svg viewBox="64 64 896 896" focusable="false" data-icon="reload" width="1em" height="1em" fill="currentColor" aria-hidden="true"><path d="M909.1 209.3l-56.4 44.1C775.8 155.1 656.2 92 521.9 92 290 92 102.3 279.5 102 511.5 101.7 743.7 289.8 932 521.9 932c181.3 0 335.8-115 394.6-276.1 1.5-4.2-.7-8.9-4.9-10.3l-56.7-19.5a8 8 0 00-10.1 4.8c-1.8 5-3.8 10-5.9 14.9-17.3 41-42.1 77.8-73.7 109.4A344.77 344.77 0 01655.9 829c-42.3 17.9-87.4 27-133.8 27-46.5 0-91.5-9.1-133.8-27A341.5 341.5 0 01279 755.2a342.16 342.16 0 01-73.7-109.4c-17.9-42.4-27-87.4-27-133.9s9.1-91.5 27-133.9c17.3-41 42.1-77.8 73.7-109.4 31.6-31.6 68.4-56.4 109.3-73.8 42.3-17.9 87.4-27 133.8-27 46.5 0 91.5 9.1 133.8 27a341.5 341.5 0 01109.3 73.8c9.9 9.9 19.2 20.4 27.8 31.4l-60.2 47a8 8 0 003 14.1l175.6 43c5 1.2 9.9-2.6 9.9-7.7l.8-180.9c-.1-6.6-7.8-10.3-13-6.2z"></path></svg>
                    </div>
                </div>
                <ConfigProvider locale={zhCN}>
                    <Table
                        columns={columns as any}
                        dataSource={store.comment.commentList.map((item, index) => {
                            item.key = item.id;
                            store.comment.commentList.map(item2 => {
                                if (item.parentCommentId === item2.id) {
                                    item.parentcontent = item2.content
                                }
                            })
                            return item;
                        })}
                        scroll={{ x: 2000 }}
                        rowSelection={rowSelection}
                        onChange={changePage}
                        pagination={{
                            total: store.comment.commentNum,
                            pageSizeOptions: ["8", "12", "24", "36"],
                            current: page,
                            pageSize: pageSize,
                            showTotal: () => `共 ${store.comment.commentNum} 条`,
                            showSizeChanger: true,
                        }}
                    />
                </ConfigProvider>
            </div>
        </div>
    )
}
export default observer(Comment);
