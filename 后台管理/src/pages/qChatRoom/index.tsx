import React, { useEffect, useState, useRef } from 'react';
import io, { Socket } from 'socket.io-client'
import md5 from 'md5';
import style from './index.less'
const COLORS = [
    '#e21400', '#91580f', '#f8a700', '#f78b00',
    '#58dc00', '#287b00', '#a8f07a', '#4ae8c4',
    '#3b88eb', '#3824aa', '#a700ff', '#d300e7'
];

interface IMessage {
    title?: string,
    message: string
}
let timeout = 0;
let TYPING_YIMER = 400
const ChatRoom = () => {
    const instance = useRef<Socket>();
    const [shouDialog, setShouDialog] = useState(true)
    const [username, setUsername] = useState('')
    const [content, setContent] = useState('')
    const [messages, setMessage] = useState<IMessage[]>([])
    const container = useRef<HTMLDivElement>(null)
    const [isTyping, setIsTyping] = useState(false);

    useEffect(() => {
        if (container.current) {
            container.current!.scrollTop = container.current?.scrollHeight!;
        }
    }, [messages])

    useEffect(() => {
        instance.current = io('ws://127.0.0.1:3000')


        // Whenever the server emits 'login', log the login message  //当服务器发出'login'时，记录登录消息
        instance.current.on('login', (data) => {
            // Display the welcome message  //显示欢迎信息
            setMessage(messages => [...messages, {
                message: `欢迎来到 Socket.IO Chat – `
            }, {
                message: `there's ${data.numUsers} 人`
            }])

        });

        //   // Whenever the server emits 'new message', update the chat body  //当服务器发出'new message'时，更新聊天体
        instance.current.on('new message', (data) => {
            console.log(data, 'new message');
            setMessage(messages => [...messages, {
                title: data.username,
                message: data.message
            }])
        });

        //   // Whenever the server emits 'user joined', log it in the chat body  //当服务器发出'user joined'时，将其记录到聊天主体中
        instance.current.on('user joined', (data) => {
            console.log(data, 'user join');
            setMessage(messages => [...messages, {
                message: `${data.username} 进入了聊天室`
            }, {
                message: `现在聊天室有 ${data.numUsers} 人`
            }])
        });

        //   // Whenever the server emits 'user left', log it in the chat body   //当服务器发出'user left'时，记录在聊天主体中
        instance.current.on('user left', (data) => {
            setMessage(messages => [...messages, {
                message: `${data.username} 离开了聊天室`
            }])
        });

        //   // Whenever the server emits 'typing', show the typing message  当服务器发出'typing'时，显示typing消息1.
        //   socket.on('typing', (data) => {
        //     addChatTyping(data);
        //   });
        instance.current.on('typing', (data) => {
            console.log(data, 'zhengzaishuru');

            setMessage(messages => [...messages, {
                title: data.username,
                message: '正在输入'
            }])
        });


        //   // Whenever the server emits 'stop typing', kill the typing message  //当服务器发出'stop typing'时，终止键入消息
        instance.current.on('stop typing', (data) => {
            console.log(data, 'stop typing');

            let index = messages.findIndex(item => {
                return (item.title === undefined) && (item.message === '正在输入')
            })
            console.log(index, messages);

            if (index > -1) {
                messages.splice(index, 1)
            }
            return [...messages]
        });

        instance.current.on('disconnect', () => {
            setMessage(messages => [...messages, {
                message: `您的连接断了`
            }])
        });

        instance.current.on('reconnect', () => {
            setMessage(messages => [...messages, {
                message: '你已经重新联通'
            }])
            if (username) {
                instance.current!.emit('add user', username);
            }
        });


        instance.current.on('reconnect_error', () => {
            //     log('attempt to reconnect has failed');
            setMessage(messages => [...messages, {
                message: `尝试重连失败`
            }])
        });

    }, [])

    // 添加用户
    function addUsername(e: React.KeyboardEvent) {
        if (username && e.keyCode === 13) {
            instance.current!.emit('add user', username);
            setShouDialog(false)
        }
    }
    // 发送
    function sendMessage(e: React.KeyboardEvent) {
        if (content && e.keyCode === 13) {
            instance.current!.emit('new message', content);

            // setMessage([...messages, { title: username, message: content }])
            setContent('');
        }
    }
    // 获取用户颜色
    function getUserColor(username: string) {
        let str = md5(username);
        return parseInt(str.slice(-1), 16) % 12;
    }
    // 用户正在输入
    function useInput(e: React.FormEvent) {
        if (!isTyping) {
            instance.current?.emit('typing');
            timeout = setTimeout(() => {
                instance.current?.emit('stop typing');
                setIsTyping(false)
            }, TYPING_YIMER) as unknown as number;
        } else {
            clearTimeout(timeout)
            timeout = setTimeout(() => {
                instance.current?.emit('stop typing');
                setIsTyping(false)
            }, TYPING_YIMER) as unknown as number;
        }
    }


    return <div>
        {
            shouDialog ? <div className={style.wrap}>
                <p>What`s your nickName?</p>
                <input type="text" value={username} onChange={(e) => setUsername(e.target.value)} onKeyDown={addUsername} />
            </div> : <div className={style.room}>
                <div ref={container}>{
                    messages.map((item, index) => {
                        if (!item.title) {
                            return <p key={index} className={style.tip}>{item.message}</p>
                        } else {
                            return <p key={index} className={style.message}>
                                <span style={{ color: COLORS[getUserColor(item.title)] }}>{item.title}</span>
                                <span>{item.message}</span>
                            </p>
                        }
                    })
                }</div>
                <input onInput={useInput} type="text" placeholder='Type here...' value={content} onChange={(e) => setContent(e.target.value)} onKeyDown={sendMessage} />
            </div>
        }
    </div>
}
export default ChatRoom