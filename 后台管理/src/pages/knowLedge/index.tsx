import React, { useEffect, useState } from 'react';
import styles from './index.less';
import { Button, Card, Spin, Empty, Popconfirm, ConfigProvider ,Popover} from 'antd';

import zhCN from 'antd/es/locale/zh_CN'; // zhCN下面赋值用 中文语言包；
import './index.less';
import 'antd/dist/antd.css';
import Search from '@/components/Search';
import {
  EditOutlined,
  SettingOutlined,
  CloudUploadOutlined,
  DeleteOutlined,
  PlusOutlined,
  CloudDownloadOutlined,
} from '@ant-design/icons';

import { observer } from 'mobx-react-lite';
import useStore from '@/context/useStore';
import Pagination from '@/components/Pagination';
import IDrawer from '@/components/Drawer';
import { IRootObject } from '@/types';
import BigDrawer from '@/components/BigDrawer';
const KnowLedge: React.FC = (props) => {
  const store = useStore();
  const [page, setpage] = useState(1);
  const [local, setLocal] = useState(zhCN);
  const [limit, setlimit] = useState(12);
  const [params, setParams] = useState({});
  const [loading, setLoading] = useState(true);
  const [pageNum, setPageNum] = useState(['8', '12', '24', '36']);
  const [item,setItem] =useState({})
  useEffect(() => {
    store.knowLedge.getKnowledge(page, limit, params);
    return () => {};
  }, [page, limit]);

  useEffect(() => {}, [store.knowLedge.visible,item]);


  function onChange(page: number, pageSize: number | undefined) {
    setlimit(pageSize!);
  }
  const showPopconfirm = (id: string) => {
    store.knowLedge.KnowLedgeList.map((item) => {
      if (item.id === id) {
        item.type = !item.type;
      }
    });
  };

  const handleOk = (id: string) => {
    store.knowLedge.KnowLedgeList.map((item) => {
      if (item.id === id) {
        item.type = !item.type;
      }
    });
    store.knowLedge.deleteKnowledge(id);
  };

  const handleCancel = (id: string) => {
    store.knowLedge.KnowLedgeList.map((item) => {
      if (item.id === id) {
        item.type = !item.type;
      }
    });
  };
  function handleSet(item:IRootObject) {
    store.knowLedge.visible = true; 
    setItem(item)
  }
  async function handlePublish(id: string, status: string) {
    let res = await store.knowLedge.knowledgeStatus(id, (status === 'publish' ? 'draft' : 'publish'));
    let result = await store.knowLedge.getKnowledge(page, limit, params);
  }

  return (
    <div>
      {/* 头部搜索    */}
      <Search page={page} limit={limit} />

      <div style={{ padding: '24px 12px' }} className={styles.main}>
        {/* 新建部分 */}
        <div className={styles.top}>
          <div></div>
          <div>
            <Button
              type="primary"
              onClick={() => {
                store.knowLedge.visible = true;
                setItem({}); 
                store.knowLedge.content='更新'                
              }}
            >
              <PlusOutlined  /> 新建
            </Button>
          </div>
        </div>
        {/* 图片编辑部分 */}

        <Spin size="large" spinning={loading}>
          {store.knowLedge.KnowLedgeList.length > 0 ? (
            <div className={styles.imgBox}>
              {store.knowLedge.KnowLedgeList.map((item) => {
                return (
                  <Card
                    onLoadCapture={() => {
                      setLoading(false);
                    }}
                  
                    key={item.id}
                    style={{ width: 260, marginTop: 16 }}
                    actions={[
                      <EditOutlined key="edit" />,
                      ,
                      item.status == 'publish' ? (
                        <Popover content={item.status == 'publish' ? '设为草稿' : '发布上线'} >
                   <CloudDownloadOutlined
                          onClick={() => handlePublish(item.id, item.status)}
                          key="clouddownload"
                        />
                        </Popover>
                        
                      ) : (
                        <Popover content={item.status == 'publish' ? '设为草稿' : '发布上线'} >
                        <CloudUploadOutlined
                          onClick={() => handlePublish(item.id, item.status)}
                          key="cloudupload"
                        />
                          </Popover>
                      ),

                      <SettingOutlined
                        key="setting"
                        onClick={() => handleSet(item)}
                      />,

                      <Popconfirm
                        title="确认删除"
                        visible={item.type}
                        okText="确认"
                        cancelText="取消"
                        onConfirm={() => handleOk(item.id)}
                        // okButtonProps={{ loading: confirmLoading }}
                        onCancel={() => handleCancel(item.id)}
                      >
                        <Button
                          style={{ border: 'none', top: '0.6px' }}
                          onClick={() => showPopconfirm(item.id)}
                        >
                          <DeleteOutlined key="delete" />
                        </Button>
                      </Popconfirm>,
                    ]}
                    cover={<img src={item.cover} alt="" />}
                  >
                    <h3 style={{ width: 260 }}>{item.title}</h3>
                    {item.summary && (
                      <p style={{ width: 260 }}>{item.summary}</p>
                    )}
                  
                  </Card>
                );
              })}
            </div>
          ) : (
            <ConfigProvider locale={local}>
              {' '}
              <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} />{' '}
            </ConfigProvider>
          )}
        </Spin>
        {/* 分页部分 */}
        <div className={styles.bottom}>
          <Pagination
            limit={limit}
            page={page}
            pageNum={pageNum}
            onChange={onChange}
            totals={store.knowLedge.total}
          />
        </div>
      </div>
      {/* 第一个折叠屏 */}
      <div>
        <IDrawer item={item as IRootObject}/>
       </div>
      {/* 第二个折叠屏 */}
      <div>
        <BigDrawer />
      </div>
    </div>
  );
};
export default observer(KnowLedge);
