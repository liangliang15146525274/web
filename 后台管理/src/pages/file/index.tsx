import React, { useEffect, useState } from 'react'
import useStore from '@/context/useStore'
import styles from './index.less'
import { observer } from "mobx-react-lite"
import { Upload, Row, Col, Form, Button, Input, Card, Drawer, Popconfirm, message, Pagination } from 'antd'
import { FileList } from '@/types'
import { copyText } from '@/utils/copy'
import ImageView from '@/components/ImageView'
import { InboxOutlined } from '@ant-design/icons';
const { Meta } = Card;
const { Dragger } = Upload;
interface IProp {
    name: string;
    multiple: boolean;
    action: string;
    onChange(info: any): void;
    onDrop(e: any): void;
  }
const File: React.FC = () => {
    const store = useStore()
    const [form] = Form.useForm();
    const [page, setPage] = useState(1)
    const [pageSize, setPageSize] = useState(12)
    const { fileList, total, deleteFile } = store.file
    const [file, setFile] = useState<Partial<FileList>>({})
    useEffect(() => {
        store.file.getFileList({ page, pageSize })
    }, [page, pageSize])
    const onFinish = (values: any) => {
        store.file.getFileList({ page, pageSize, originalname: values.fileName, type: values.fileType })
        console.log('Received values of form: ', values);
    };
    //判断删除框
    const [visible, setVisible] = useState(false);
    //显示侧边框
    const showLargeDrawer = (item: FileList) => {
        setVisible(true);
        setFile(item)
    };
    //取消侧边框
    const onClose = () => {
        setVisible(false);
    };
    //删除提示框确认
    const confirm = async () => {
        let result = await deleteFile(file.id!)
        if (result.statusCode === 200) {
            message.success("删除成功")
        } else {
            message.error("删除失败")
        }
    }
    //删除提示框取消
    const cancel = () => { }
    //拷贝路径
    const copy = (url: string) => {
        copyText(url)
    }
    const prop: IProp = {
        name: 'file',
        multiple: true,
        action: 'https://www.mocky.io/v2/5cc8019d300000980a055e76',
        onChange(info) {
          const { status } = info.file;
          if (status !== 'uploading') {
            console.log(info.file, info.fileList);
          }
          if (status === 'done') {
            message.success(`${info.file.name} file uploaded successfully.`);
          } else if (status === 'error') {
            message.error(`${info.file.name} file upload failed.`);
          }
        },
        onDrop(e) {
          console.log('Dropped files', e.dataTransfer.files);
        },
      };
    return (
        <div id={styles.file}>
            <div className={styles.search}>
                <Form
                    form={form}
                    name="advanced_search"
                    className="ant-advanced-search-form"
                    onFinish={onFinish}
                >
                    <Row gutter={24}>
                        <Col span={6}>
                            <Form.Item
                                name={`fileName`}
                                label={`文件名称`}
                                labelCol={{ span: 6 }}
                            >
                                <Input placeholder="请输入文件名称" />
                            </Form.Item>
                        </Col>
                        <Col span={6} >
                            <Form.Item
                                name={`fileType`}
                                label={`文件类型`}
                                labelCol={{ span: 6 }}
                            >
                                <Input placeholder="请输入文件类型" />
                            </Form.Item>
                        </Col>
                    </Row>
                    <Row>
                        <Col span={24} style={{ textAlign: 'right' }}>
                            <Button type="primary" htmlType="submit">
                                搜索
                            </Button>
                            <Button
                                style={{ margin: '0 8px' }}
                                onClick={() => {
                                    form.resetFields();
                                }}
                            >
                                重置
                            </Button>
                        </Col>
                    </Row>
                </Form>
            </div>
            <div className={styles.top}>
                <Dragger {...prop} style={{ background: '#fff', width: '100%' }}>
                    <p className="ant-upload-drag-icon">
                        <InboxOutlined />
                    </p>

                    <p className="ant-upload-text">点击选择文件或将文件拖拽到此处</p>
                    <p className="ant-upload-hint">文件将上传到 OSS, 如未配置请先配置</p>
                </Dragger>
            </div>
            <div className={styles.context}>
                <Row gutter={[16, 16]}>
                    {
                        fileList.map(item => {
                            return <Col span={6} key={item.id} onClick={() => showLargeDrawer(item)}>
                                <Card
                                    hoverable
                                    style={{ width: "100%" }}
                                    cover={<img alt="example" src={item.url} />}
                                >
                                    <Meta title={item.originalname} description={item.createAt} />
                                </Card>
                            </Col>
                        })
                    }
                </Row>
                <div className={styles.pagination}>
                    <Pagination onChange={(current) => {
                        setPage(current)
                    }}
                        current={page}
                        onShowSizeChange={(page, size) => {
                            setPageSize(size)
                        }}
                        pageSize={pageSize}
                        pageSizeOptions={["8", "12", "24", "36"]}
                        defaultCurrent={1}
                        total={total}
                        showSizeChanger={true} />
                </div>
                <Drawer
                    title={`文件信息`}
                    placement="right"
                    width="592px"
                    onClose={onClose}
                    visible={visible}
                    closable={true}
                >
                    <ImageView>
                        <img className={styles.img} src={file.url} alt="" />
                    </ImageView>
                    <div className={styles.outer}><p>文件名称：</p><p>{file.originalname}</p></div>
                    <div className={styles.outer}><p>存储路径：</p><p>{file.filename}</p></div>
                    <div className={styles.outer}><div>文件类型：{file.type}</div><div>文件大小：{file.size}</div> </div>
                    <div className={styles.outer}><span>访问链接</span> <div>
                        <div onClick={() => copy(file.url!)}>{file.url}</div>
                        <span onClick={() => copy(file.url!)}>复制</span>
                    </div></div>
                    <div className={styles.footer}>
                        <Popconfirm
                            placement="topRight"
                            title="确认删除这个文件？"
                            onConfirm={confirm}
                            onCancel={cancel}
                            okText="Yes"
                            cancelText="No"
                        >
                            <Button danger style={{ marginLeft: "10px" }}>删除</Button>
                        </Popconfirm>
                        <Button onClick={onClose}>关闭</Button> </div>
                </Drawer>
            </div>
        </div>
    )
}
export default observer(File)
