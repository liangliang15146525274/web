import {
    MenuUnfoldOutlined, MenuFoldOutlined, UserOutlined, DashboardOutlined, FormOutlined,
    SnippetsOutlined, BookOutlined, StarOutlined, MessageOutlined, CopyOutlined, TagOutlined,
    MailOutlined, FolderOpenOutlined, SearchOutlined, ProjectOutlined, SettingOutlined, DownOutlined
} from '@ant-design/icons';
export const urlMatching = [
    {
        title: '工作台',
        to: '/',
        icon: DashboardOutlined
    },
    {
        title: '文章管理',
        icon: SnippetsOutlined,
        children: [
            {
                title: '所有文章',
                to: '/article',
                icon: CopyOutlined
            }, {
                title: '分类管理',
                to: '/article/category',
                icon: CopyOutlined
            }, {
                title: '标签管理',
                to: '/article/tags',
                icon: TagOutlined
            },
        ]
    },
    {
        title: '页面管理',
        to: '/page',
        icon: FormOutlined,
    },

    {
        title: '知识小册',
        to: '/knowledge',
        icon: BookOutlined,
    },

    {
        title: '海报管理',
        to: '/poster',
        icon: StarOutlined,
    },

    {
        title: '评论管理',
        to: '/comment',
        icon: MessageOutlined,
    },

    {
        title: '邮件管理',
        to: '/mail',
        icon: MailOutlined,
    },

    {
        title: '文件管理',
        to: '/file',
        icon: FolderOpenOutlined,
    },

    {
        title: '搜索记录',
        to: '/search',
        icon: SearchOutlined,
    },

    {
        title: '访问统计',
        to: '/view',
        icon: ProjectOutlined,
    },

    {
        title: '用户管理',
        to: '/user',
        icon: UserOutlined,
    },

    {
        title: '系统设置',
        to: '/setting',
        icon: SettingOutlined,
    }
]
export const urlMatch: { [key: string]: string } = {
    article: '所有文章',
    category: '分类管理',
    tags: '标签管理',
    page: '页面管理',
    knowledge: '知识小册',
    poster: '海报管理',
    comment: '评论管理',
    mail: '邮件管理',
    file: '文件管理',
    search: '搜索记录',
    view: '访问统计',
    user: '用户管理',
    setting: '系统设置'
}