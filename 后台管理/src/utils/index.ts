import Cookie from 'js-cookie'
const key = "authorization"
export function setToken(value: string) {
    let now = +new Date()
    let expires = new Date(now+24*60*60*1000)
    Cookie.set(key,value,{expires})
}
export function getToken() {
    return Cookie.get(key)
}
export function removeToken() {
    Cookie.remove(key)
}
