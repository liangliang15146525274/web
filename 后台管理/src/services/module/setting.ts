import { Analytics, Seo, Smtp, SystemSettings } from '@/types/module/setting'
import { request } from 'umi'
export const getSetting = () => {
    return request("/api/setting/get", { method: "POST" })
}
export const commitModification = (options: SystemSettings | Seo | Analytics | Smtp) => request("/api/setting", { method: "POST", data: options })

export const testEmail = (options: {
    subject: string,
    text: string,
    to: string
}) => request("/api/smtp", { method: "POST", data: options })
