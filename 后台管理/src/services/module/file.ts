import {request} from 'umi'
export const getFileList = ()=>{
    return request('/api/file?page=1&pageSize=12')
}