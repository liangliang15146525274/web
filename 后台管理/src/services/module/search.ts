import { request } from 'umi';
import { ISearchParams } from "@/types";

//获取搜索记录数据与搜索功能
export function getSearchList(page: number = 1, pageSize: number = 12, params: Partial<ISearchParams> = { }) {
    return request(`/api/search?page=${page}&pageSize=${pageSize}`, { params })
}


//删除
export function delSearchList(id: string) {
    return request(`/api/search/${id}`, {
        method: 'DELETE',
    })
}
