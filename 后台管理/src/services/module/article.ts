import { IArticleItem, ICategoryItem, IPageList } from '@/types'
import { request } from 'umi'
// 获取分类
export const getCategoryList = function () {
    return request('/api/category')
}
// 获取标签
export const getTagList = function () {
    return request('/api/tag')
}
// 获取页面列表
export const getArticleList = function (payload: { page: number, pageSize?: number, title?: string, status?: string, category?: string, }) {
    return request('/api/article', { params: payload })
}
// 删除
export const getDelArticle = function (id: string) {
    return request(`/api/article/${id}`, { method: 'DELETE' })
}
// 发布 下线
export const getOutArticle = function (data: { id: string, status?: string, isRecommended?: boolean }) {
    return request(`/api/article/${data.id}`, { method: 'PATCH', data: { status: data.status, isRecommended: data.isRecommended } })
}
// 删除分类
export const getDelCategory = function (id: string) {
    return request(`/api/category/${id}`, { method: 'DELETE' })
}
// 添加分类
export const getAddCategory = function (label: string, value: string) {
    return request(`/api/category`, { method: 'POST', data: { label, value } })
}
// 编辑分类
export const getEditCategory = function (id: string, label: string, value: string) {
    return request(`/api/category/${id}`, { method: 'PATCH', data: { label, value } })
}
// 删除标签
export const getDelTag = function (id: string) {
    return request(`/api/tag/${id}`, { method: 'DELETE' })
}
// 添加标签
export const getAddTag = function (label: string, value: string) {
    return request(`/api/tag`, { method: 'POST', data: { label, value } })
}
// 编辑标签
export const getEditTag = function (id: string, label: string, value: string) {
    return request(`/api/tag/${id}`, { method: 'PATCH', data: { label, value } })
}
// 发布 文章 草稿
export const getPublish = function (data: Partial<IArticleItem>) {
    return request(`/api/article`, { method: "POST", data: data })
}
//编辑文章数据详情
export const getDetail = function (id: string) {
    return request(`/api/article/${id}`)
}
// 编辑文章发布
export const getEditPub = function (id: string, data: IArticleItem) {
    return request(`/api/article/${id}`, { method: 'PATCH', data: data })
}
// 发布 页面 草稿
export const getPubPage = function (data: Partial<IPageList>) {
    return request(`/api/page`, { method: "POST", data: data })
}
//编辑文章数据详情
export const getPageDetail = function (id: string) {
    return request(`/api/page/${id}`)
}
// 编辑页面发布
export const getEditPage = function (id: string, data: IPageList) {
    return request(`/api/page/${id}`, { method: 'PATCH', data: data })
}
