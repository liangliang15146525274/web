import useStore from "@/context/useStore"
import React from 'react'
import { Button, Drawer, Form, Input, Select, Switch } from "antd"
import { useState } from "react"
const { Option } = Select;
const setDrawer: React.FC = (props) => {
    const store = useStore()
    const [form] = Form.useForm()
    const [visible, setVisible] = useState(true)
    const [cover, setCover] = useState('');
    const [isCommentable, setIsCommentable] = useState(false)
    const [isRecommended, setIsRecommended] = useState(false)
    const onClose = () => {
        setVisible(false)
    };
    return (
        <>
        <Drawer
            title="文章设置"
            placement={'right'}
            closable={true}
            onClose={onClose}
            visible={visible}
            key={'id'}
            width={'480px'}
            footer={
                [<Button type='primary' onClick={() => {
                    setVisible(false)
                }} htmlType="submit">确认</Button>]
            }
            footerStyle={{ textAlign: 'right' }}
        >

            <Form
                key='from'
                form={form}
            >
                <Form.Item name="summary" label="文章摘要" key='1'>
                    <Input.TextArea defaultValue={store.article.detail?.summary} style={{ height: '142px' }}></Input.TextArea>
                </Form.Item>
                <Form.Item name="password" label="访问密码" key='2'>
                    <Input.Password defaultValue={store.article.detail?.needPassword ? store.article.detail?.password : ''} placeholder="" />
                </Form.Item>
                <Form.Item name="totalAmount" label="付费查看" key='3'>
                    <Input.Password defaultValue={store.article.detail?.totalAmount} placeholder="" />
                </Form.Item>
                <Form.Item name="isCommentable" label="开启评论" key='4'>
                    <Switch checked={isCommentable} onChange={e => setIsCommentable(!isCommentable)} />
                </Form.Item>
                <Form.Item name="isRecommended" label="首页推荐" key='5'>
                    <Switch checked={isRecommended} onChange={e => setIsRecommended(!isRecommended)} />
                </Form.Item>
                <Form.Item name="category" label="选择分类" key='6'>
                    <Select key='100' defaultValue={store.article.detail?.category ? store.article.detail?.category.label : null} >{
                        store.article.categoryList.map(item => {
                            return <Select.Option key={item.id} value={item.id}>{item.label}</Select.Option>
                        })
                    }</Select>
                </Form.Item>
                <Form.Item name="tags" label="选择标签" key='7'>
                    <Select
                        mode="tags"
                        size={'large'}
                        style={{ width: '100%' }}
                        defaultValue={store.article.detail?.tags.length ? store.article.detail?.tags.map(item => item.label) : []}
                    >
                        {store.article.tagList.length > 0 ? store.article.tagList.map(item => {
                            return <Option key={item.id} value={item.id}>{item.label}</Option>
                        }) : ''}
                    </Select>

                </Form.Item>

                <Form.Item name="cover" label="文章封面" key='8'>
                    <div>
                        <img src={cover} alt="" style={{
                            display: 'flex',
                            justifyContent: 'center',
                            alignItems: 'center',
                            height: '180px',
                            marginBottom: '16px',
                            color: '#888',
                            backgroundColor: '#f5f5f5',
                        }} />
                    </div>
                    <Input type="text" value={cover} placeholder="或输入外部链接" style={{ marginBottom: '16px', }} onChange={e => setCover(e.target.value)} />
                    <Button onClick={() => {
                        let values = form.getFieldsValue();
                        form.setFieldsValue({ ...values, cover: '' })
                        setCover('');
                    }}>移除</Button>
                </Form.Item>

            </Form>
        </Drawer></>
    )
}
export default setDrawer