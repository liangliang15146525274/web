import React, { useEffect } from 'react'
import Viewer from 'viewerjs'
import "viewerjs/dist/viewer.css"
const ImageView: React.FC<{isShowWidth?:boolean}> = (props) => {
    const dom = React.createRef<HTMLDivElement>();
    useEffect(()=>{
        let viewer = new Viewer(dom.current!)
        let ob = new MutationObserver(()=>{
            viewer.update()
        })
        ob.observe(dom.current!,{
            childList:true,
            subtree:true
        })
        return ()=>{
            viewer.destroy()
            ob.disconnect()
        }
    })
    return <div ref={dom} style={{width:"100%",height:props.isShowWidth?"100%":"auto"}}>
        {props.children}
    </div>
}
export default ImageView