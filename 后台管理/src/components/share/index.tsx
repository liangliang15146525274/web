import { Modal, Button } from 'antd'
import * as echarts from 'echarts';
import { useEffect } from 'react';
import ReactDOM from 'react-dom';
// import 
export const Share: React.FC = (props) => {

    useEffect(() => {
        let chartDom = document.getElementById('main');
        let myChart = echarts.init(chartDom as HTMLCanvasElement | HTMLDivElement);


        let symbolSize = 20;
        let data = [[40, -10], [-30, -5], [-76.5, 20], [-63.5, 40], [-22.1, 50]];

        let option: echarts.EChartOption = {
            title: {
                text: 'Try Dragging these Points',
                left: 'center'
            },
            tooltip: {
                triggerOn: 'none',
                // formatter: function (params: { data: number[]; }) {
                //     return 'X: ' + params.data[0].toFixed(2) + '<br>Y: ' + params.data[1].toFixed(2);
                // }
            },
            grid: {
                top: '8%',
                bottom: '12%',
            },
            xAxis: {
                min: -100,
                max: 70,
                type: 'value',
                axisLine: { onZero: false }
            },
            yAxis: {
                min: -30,
                max: 60,
                type: 'value',
                axisLine: { onZero: false }
            },
            dataZoom: [
                {
                    type: 'slider',
                    xAxisIndex: 0,
                    filterMode: 'none'
                },
                {
                    type: 'slider',
                    yAxisIndex: 0,
                    filterMode: 'none'
                },
                {
                    type: 'inside',
                    xAxisIndex: 0,
                    filterMode: 'none'
                },
                {
                    type: 'inside',
                    yAxisIndex: 0,
                    filterMode: 'none'
                }
            ],
            series: [
                {
                    id: 'a',
                    type: 'line',
                    smooth: true,
                    symbolSize: symbolSize,
                    data: data
                }
            ]
        };

        option && myChart.setOption(option, true);
    }, [])

    function handleCancel() {
        ReactDOM.unmountComponentAtNode(document.querySelector('#share-dislog')!)
    };

    return <Modal
        visible={true}
        title="访问统计"
        onCancel={handleCancel}
        footer={null}
    >
        <div id='main' style={{ height: "300px" }}></div>
    </Modal>
}

export default function share() {
    let shareDislog = document.querySelector('#share-dislog');
    if (!shareDislog) {
        shareDislog = document.createElement('div');
        shareDislog.id = 'share-dislog';
        document.body.appendChild(shareDislog);
    }
    ReactDOM.render(<Share />, shareDislog)
}
