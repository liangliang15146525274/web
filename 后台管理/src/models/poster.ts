import {makeAutoObservable} from 'mobx';

class Poster{
    constructor(){
        makeAutoObservable(this);
    }
}

export default Poster;
