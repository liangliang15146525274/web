import { makeAutoObservable } from 'mobx';
import { getViewList, delViewList } from '@/services';
import { IViewList, IViewParams } from "@/types";
import { message } from "antd";
class View {
    viewList: Array<IViewList> = [];
    total: number = 0;
    constructor() {
        makeAutoObservable(this);
    }
    async getViewList(page: number, pageSize: number, params: Partial<IViewParams> = {}) {
        let result = await getViewList(page, pageSize,params);
        if (result.data) {
            this.viewList = result.data[0];
            this.total = result.data[1];
        }
        return result.data;
    }
    
    async delViewList(prarms: string[],page: number, pageSize: number, params: Partial<IViewParams> = {}) {
        message.loading('操作中');
        Promise.all(prarms.map(id => delViewList(id)))
            .then(res => {
                message.destroy();
                message.success('操作成功');
                this.getViewList(page, pageSize,params);
            })
            .catch(err => {
                message.error('操作失败');
                this.getViewList(page, pageSize,params);
            })
    }
}
export default View;
