import { makeAutoObservable } from 'mobx';
import { delSearchList, getSearchList } from '@/services';
import { ISearchList, ISearchParams } from "@/types";
import { message } from "antd";
class Search {
    searchList: Array<ISearchList> = [];
    total: number = 0;
    constructor() {
        makeAutoObservable(this);
    }
    async getSearchList(page: number, pageSize: number, params: Partial<ISearchParams> = { }) {
        let result = await getSearchList(page, pageSize, params);
        if (result.data) {
            this.searchList = result.data[0];
            this.total = result.data[1];
        }
        return result.data;
    }

    async delSearchList(arr: string[], page: number, pageSize: number, params: Partial<ISearchParams> = { }) {
        message.loading('操作中');
        Promise.all(arr.map(id => delSearchList(id)))
            .then(res => {
                message.destroy();
                message.success('操作成功');
                this.getSearchList(page, pageSize, params);
            })
            .catch(err => {
                message.error('操作失败');
                this.getSearchList(page, pageSize, params);
            })
    }
}
export default Search;
