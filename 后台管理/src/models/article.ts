import { getAddCategory, getAddTag, getArticleList, getCategoryList, getDelArticle, getDelCategory, getDelTag, getDetail, getEditCategory, getEditPage, getEditPub, getEditTag, getOutArticle, getPageDetail, getPublish, getPubPage, getTagList } from '@/services';
import { IArticleItem, ICategoryItem, ITagItem } from '@/types/module/article';
import { makeAutoObservable } from 'mobx';
import { message } from 'antd'
import { IPageList } from '@/types';

class Article {
    articleList: any = []
    categoryList: ICategoryItem[] = []
    tagList: ITagItem[] = []
    detail:IArticleItem|null=null
    pageDetail:IPageList|null=null
    articleCount = 0
    constructor() {
        makeAutoObservable(this);
    }
    // 分类列表
    async getCategoryList() {
        let result = await getCategoryList();
        if (result.statusCode === 200) {
            this.categoryList = result.data;
        }
    }
    // 标签列表
    async getTagList() {
        let result = await getTagList();
        if (result.statusCode === 200) {
            this.tagList = result.data;
        }
    }
    // 页面列表
    async getArticleList(data: { page: number, pageSize?: number, title?: string, status?: string, category?: string }) {
        let result = await getArticleList(data);
        if (result.statusCode === 200) {
            this.articleList = result.data[0];
            this.articleCount = result.data[1]
        }
    }
    // 删除
    async getDelArticle(id: string) {
        let result = await getDelArticle(id);
        return result
    }
    // 下线
    async getOutArticle(obj: { id: string, status?: string, isRecommended?: boolean }) {
        let result = await getOutArticle(obj);
        return result
    }
    // 批量下线
    async getOutArticles(array: React.Key[], status?: string) {
        array.forEach(async item => {
            let result = await getOutArticle({ id: item as string, status });
            if (result.statusCode === 200) {
                message.success('操作成功');
                this.getArticleList({ page: 1, pageSize: 12 })
            }
        })
    }
    // 批量焦点
    async getUpList(array: React.Key[], isRecommended?: boolean) {
        array.forEach(async item => {
            let result = await getOutArticle({ id: item as string, isRecommended });
            if (result.statusCode === 200) {
                message.success('操作成功');
                this.getArticleList({ page: 1, pageSize: 12 })
            }
        })
    }

    // 添加分类
    async getAddCategory(label: string, value: string) {
        let result = await getAddCategory(label, value);
        return result
    }
    // 编辑分类
    async getEditCategory(id: string, label: string, value: string) {
        let result = await getEditCategory(id, label, value);
        return result
    }
    // 删除分类
    async getDelCategory(id: string) {
        let result = await getDelCategory(id);
        return result
    }

    // 添加标签
    async getAddTag(label: string, value: string) {
        let result = await getAddTag(label, value);
        return result
    }
    // 编辑标签
    async getEditTag(id: string, label: string, value: string) {
        let result = await getEditTag(id, label, value);
        return result
    }
    // 删除标签
    async getDelTag(id: string) {
        let result = await getDelTag(id);
        return result
    }
    // 文章发布
    async getPublish(obj:Partial<IArticleItem>){
        let result=await getPublish(obj);
        return result
    }
    // 详情数据
    async getDetail(id:string){
        let result=await getDetail(id);
        if (result.statusCode === 200) {
            this.detail=result.data
        }
    }
    // 文章编辑发布
    async getEditPub(id:string,data:IArticleItem){
        let result=await getEditPub(id,data);
        return result
    }
    // 文章发布
    async getPubPage(obj:Partial<IPageList>){
        let result=await getPubPage(obj);
        return result
    }
    // 详情数据
    async getPageDetail(id:string){
        let result=await getPageDetail(id);
        if (result.statusCode === 200) {
            this.pageDetail=result.data
        }
    }
    // 文章编辑发布
    async getEditPage(id:string,data:IPageList){
        let result=await getEditPage(id,data);
        return result
    }

}

export default Article;
