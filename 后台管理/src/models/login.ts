import {makeAutoObservable} from 'mobx';
import { member } from '@/services';
import { Registry } from '@/types';
class Login{
    constructor(){
        makeAutoObservable(this);
    }
    async member(data:Registry,url:string){
        let result = await member(data,url);
        return result;
    }
}

export default Login;
