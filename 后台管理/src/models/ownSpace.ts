import {makeAutoObservable} from 'mobx';

class OwnSpace{
    constructor(){
        makeAutoObservable(this);
    }
}

export default OwnSpace;
