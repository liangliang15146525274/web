import { deleteEmail, getEmailList } from '@/services';
import { mailObject, Params } from '@/types';
import { makeAutoObservable , runInAction} from 'mobx';
import {message} from 'antd'
import React from 'react';
class Mail {
    emailLisr:mailObject[] = []
    emailTotal:number = 0
    isSucceed:boolean = false
    constructor() {
        makeAutoObservable(this);
    }
    async getEmailList(payload:Params) {
        let result = await getEmailList(payload)
        if(result.statusCode ===200){
            runInAction(()=>{
                this.emailTotal = result.data[1]            
                this.emailLisr = result.data[0]
            })
        }
    }
    changeIsSucceed(){
        this.isSucceed=false
    }
    async deleteEmail(id:string){
        let result =await deleteEmail(id)
        if(result.statusCode ===200 ){
            this.isSucceed=true
            return result
        }
    }
    async deleteEmails(ids:React.Key[]){
        Promise.all(ids.map(id=>deleteEmail(id as string))).then(val=>{
            message.destroy();
            message.success('批量删除成功');
            this.getEmailList({page:1,pageSize:12})
        }).catch(err=>{
            message.success('批量删除成功');
            this.getEmailList({page:1,pageSize:12})
        })       
    }
}

export default Mail;
