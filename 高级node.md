  #### node 
  nodejs蹲从的是commonjs规范 ，只能用在服务端

  ### 规范
  引入：require 抛出 ：exports module.exports 
  exports 不能直接赋值，可以通过 来添加方法 exports.num
  module.exports 可以直接赋值
  exports 是module.exports的一个引用
  
  <!-- js数据类型 -->
  Number String Boolean Null undefiend Symbol
  前6中是简单类型，放在内存的栈里面
   object(Array,Function)
   object.prototype.toString.call(null,变量)
 最后的一个引用类型，放在内存的堆里面

 ### nodejs执行机制
 一个js文件就是一个模块 nodejs在执行的时候，会在每一个模块
 的外边包裹一个函数 同时吧exports require module 
 __filename  __dirname当参数传过来

 function (exports.require,module,__filename, __dirname){
    const obj=require("./sum")
    console.log(obj); //{sum:function(){},maxs:function(){}}
    let max=obj.maxs(1,2,3,4,5)
    console.log(max)

    console.log(arguments) //函数的形参
    console.log(arguments.callee+"") //获取函数本身 
 }

 #### npm 
 npm(node package manager) 包管理器。多个文件放在一个文件夹下边
 这个文件夹下边有个package.json的文件，这个文件叫包描速文件

 #### 安装
 本地安装
 开发环境 npm install包名 --save-dev -D node-sass 在
 package.json 里面的dependenciesDev这个字段里面
 
 2. 生成环境 npm install 包名--save -S 在package.json
 的dependencies这个字段里面
+l
 - 全局npm install 包名 -g 查看全局安装包的路径：npm root -g

 #### 安装的流程 npm i axios -S 
 1.先去对应 的镜像源查找
  查看镜像源  npm config get registry
 设置镜像源  npm config set registry 镜像源的地址

- 官方：http://registry.npmjs.org/
- 淘宝：http://registry.npm.taobao.org/

2. 如果找到了这个包，下载对应的压缩包到缓存目录里面 

查看缓存目录  npm config get cache

3. 解压压缩包到对应的目录里面 

 查看解压目录  npm config get prefix

##### commonjs查找机制
1. 路径里面是否有./ ../,如果有 ./ ../说明要找的是一个文件，没有要找的是一个包
2. 先在当前目录下边查找这个包
3. 如果找到了，进到包里面找index.js，如果没有index.js，就找package.json里面的main字段
4. 如果没有找到，会逐级向上查找，直到当前盘符。
5. 如果盘符里面也没有，就去全局里面找。（环境变量NODE_PATH）

### node 做什么的  执行js代码的 

#### Object.keys()  Object.values() Object.entries()
 Object.keys //获取他的健，
 Object.values //获取他的值
 Object.entries//获取他的健和值

#### 实现call apply bind 改变this指向的
call 传参的时候一个一个传递 
apply 要传递一个数组
call apply与bind的区别 前俩个改变this指向之后立即执行 bind的改变this之后不会立即执行
如果call apply bind 传个null进去返回的是window  如果在node的js里面返回的是global

let obj={  //小例子
  name:"小黑"
};
let obj1={
  name:"小花",
  fn: function(...arr){
    console.log(this,this.name,arr);// 这个就是吧参数全部展开传过来了
  }
}
obj1.fn()
obj1.fn.call(obj,1,2,3,4,5,6)
obj1.fn.apply(obj,[1,2,3,4,5,6])
obj1.fn.bind(obj,1,2,3,4,5,6)(1,2,3,4,5,6,7,8,9) //bind就是给他合并了

concat() 康凯特 是合并多个数组的



#### 命令行 
新创建一个文件 index.js 文件 在头部上写 #! /usr/bin/env node
2.生成一个package.json 在scripts下面生成 bin的         "bin": {
                                                       "bw": "./index.js"
                                                         },
通过npm link 生成 cmd 文件 如果报错 可以通过(管理员身份运行) npm install -g 生成一个全局包 
4.通过 process.argv 查看路径

##### commander 命令行里面的一个框架 指令的意思

const {program}=require("commander) commander是一个对象他对象里面的参数有program
program.parse(process.argv) 解析他的process.argv的参数
npm --help 是帮助命令 简写 -h
.option 里面是放参数的名字 
.option(flag,flag描述)
长flag是 --help
短flag是 -h
长短flag之间可以用空格隔开 也可以用逗号隔开 也可以用 | 隔开

<> 参数是必选的就是不传参数就报错
[] 参数是可选的不传也可以 传也可以不会报错

####
const option = program.opts();
option 他拿到的是对象{addFile:}

#### .command 也是一个参数 里面传create的参数 他是子命令 <file>后面传个必传参数
### .action是获取.command的子参数的命令的 回调函数他第一个参数是name获取你子命令的参数       第二个参数是获取长 flag 是个对象{} 

####  existsSync 判断有没有后缀名

### git安装 
第一步 配置用户 
git config --golobal user.name "用户名" 别写中文
git config --golobal user.email "用户邮箱"

然后第二部查看 git config --list 查看你的用户名跟邮箱

之后gitee 注册码云账户

第三部 ssh-keygen 生成一个公钥跟秘钥
（c:\User\shou/.ssh）路径吧这个路径找到
gitee 点击设置 在点击ssh公钥
+ 新建仓库

然后点击克隆下载 ssh 链接复制

然后git clone 复制ssh 秘钥链接

clone 之后 文件夹里面会有一个 .git隐藏文件夹 
这个.git文件夹叫本地版本库(记录你的每一个文件发生变化)

执行第一个命令 
git add .
git commit -m "描述" 
git pull origin master 从远端拉去代码(团队协作的时候用他)
git push origin master 吧本地的代码推送到远端去

创建分支 git checkout -b 分支名(创建并切换分支名)

### sql语句
> 添加
insert into 表明 (字段1,字段2,字段3,...) values (value1,value2,value3,...)
> 删除
delete from 表名 where id=?
> 修改
update 表名 set 字段1=?,字段2=?,字段3=? where id=?
> 查询全部
    select */列名  from 表名 
> 查询总数
    select count(*) from <表名>
> 按条件查询
    select */列名 from 表名 where 字段 = ? and/or
> 排序查询
  select */列名 from 表名 [条件语句] limit 起始下标，查询条数 
> 倒序查询
 select */列名 from 表名 order by 列名 desc
```
> 模糊查询
 select */列名 from 表名 条件 列名 like %{要匹配的内容}% 


react 里面的受控组件
value={你的数据名}
name="你的数据名"
onChange={this.handleChangeInput}

拿到值是 [e.target.name]:e.target.value


打开egg官网
第一步 打开路由 右面有个表单效验 config/config.default.js
  security : {
  csrf: false
};


第二步开始连接数据库MYSQL
第一步下载包 npm i --save egg-mysql
第二步// config/plugin.js 开始插件
mysql = {
  enable: true,
  package: 'egg-mysql',
};

第三部// config/config.${env}.js
mysql = {
  // 单数据库信息配置
  client: {
    // host
    host: 'mysql.com',
    // 端口号
    port: '3306',
    // 用户名
    user: 'test_user',
    // 密码
    password: 'test_password',
    // 数据库名
    database: 'test',
  },
  // 是否加载到 app 上，默认开启
  app: true,
  // 是否加载到 agent 上，默认关闭
  agent: false,
};

第三部RESTful API   开启validate插件
// config/plugin.js

validate = {
  enable: true,
  package: 'egg-validate',
};

middleware 创建一个中间件的文件夹
error_handler.js

// app/middleware/error_handler.js
module.exports = () => {
  return async function errorHandler(ctx, next) {
    try {
      await next();
    } catch (err) {
      // 所有的异常都在 app 上触发一个 error 事件，框架会记录一条错误日志
      ctx.app.emit('error', err, ctx);

      const status = err.status || 500;
      // 生产环境时 500 错误的详细错误内容不返回给客户端，因为可能包含敏感信息
      const error = status === 500 && ctx.app.config.env === 'prod'
        ? 'Internal Server Error'
        : err.message;

      // 从 error 对象上读出各个属性，设置到响应中
      ctx.body = { error };
      if (status === 422) {
        ctx.body.detail = err.errors;
      }
      ctx.status = status;
    }
  };
};


加密 密码  是 第一步extend  创建文件夹 
创建一个helper.js
const crypot =require（"crypto"）
module.exports={
     pwdcrypto(password){
      return crypto.createHash("md5")
             .update(password)
             .digest('hex'); 
     }
}

ctx.helper.pwdcrypto(password) 要加密的密码

验证码npm i svg-captcha 下载包 npm官网也有
router.get("/getcaptcha",controller.user.getcaptcha) 创建路由接口

在user下使用
let svgCaptcha = require('svg-captcha');在user的下引入模块 
async getcaptcha(){
         const { ctx } = this;
         let captcha = svgCaptcha.create({
            size: 4 ,
            noise: 1,
            color: true,
            width: 100,
            height:50,
            fontSize:50,
         }); 
         ctx.session.captcha = captcha.text;
         ctx.response.type='svg';
         ctx.body=captcha.data;
     }

在登录页面判断一下
if(captcha.toUpperCase()!=ctx.session.captcha.toUpperCase()){
 ctx.body = {
                code:3,
                mes:"验证码不一致"
            }
            return
}

FileeReader 上传文件获取文件信息
readAsDateURL 转成base64格式的

ctx.request.files 获取当前文件信息
multipart ：{ mode：file} 设置文件类型

用fse.move方法 移动的方法
filepath是他的图片路径 给他放到this.config.UPLOAD_URL+加是他的图片名字

获取图片的后缀名 path模块里面的
const path= require（"path"） extname//后缀名
const path= require（"path"） basename//图文文件名

  //node高级  排序
 async getlist(page=1,pageSzie=5,name,type,sorts){
      let startIndex=(page-1)*pageSzie;
      let arr=["asc","desc"]
      let sql=`select * from shoplist where name  like '%${name}%' order by ${type}*1 ${arr[sorts]}  limit ${startIndex},${pageSzie}`
      let total=await this.app.mysql.query(`select count(*) from shoplist where name like "%${name}%"`)
     let res=await this.app.mysql.query(sql)
     return {
         total:total[0]["count(*)"],
         data:res
     }
  }










