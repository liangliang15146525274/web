import { defineConfig } from 'umi';

export default defineConfig({
  nodeModulesTransform: {
    type: 'none',
  },
  title: "小楼又清风",
  fastRefresh: {},
  antd: {},
  dva: {
    immer: true,
    hmr: true,
  },
  locale: {},
  publicPath: process.env.NODE_ENV === 'production' ? '/1812A/liubo/fantasticit/' : '/',
  base:process.env.NODE_ENV === 'production' ? '/1812A/liubo/fantasticit' : '/',
});
