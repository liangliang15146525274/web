import {
    getFuzzyData,
    getknowledge,
    getknowledgedetailsdata,
    getknowledgedetailsTowdata,
    getknowledgedetailsDataONE
} from '@/services';

import {
    FuzzyData,
    RootObject,
    ClassifiCation,
    Knowledgedetails,
    KnowDetailsTwoData,
    KnowDetailsTwoDataRigth
} from '@/types';

import { Effect, ImmerReducer, Reducer, Subscription } from 'umi';

export interface KnowledgeModelState {
    FuzzyData: FuzzyData[];
    knowledgeData: RootObject[];
    knowledgedetails: Knowledgedetails[];
    knowledgedetailsTowdata: Partial<KnowDetailsTwoData>;
    knowledgedetailsDataONE: Partial<KnowDetailsTwoDataRigth>;
}

export interface KnowledgeModelType {
    namespace: 'knowledge';
    state: KnowledgeModelState;
    effects: {
        getFuzzyData: Effect;
        getknowledges: Effect;
        getknowledgedetailsdata: Effect;
        getknowledgedetailsTowdata: Effect;
        getknowledgedetailsDataONE: Effect;
    };
    reducers: {
        save: Reducer<KnowledgeModelState>;
    };
}

const IndexModel: KnowledgeModelType = {
    namespace: 'knowledge',

    state: {
        knowledgeData: [],
        knowledgedetails: [],
        knowledgedetailsTowdata: {},
        knowledgedetailsDataONE: {},
        FuzzyData: []
    },

    effects: {
        * getFuzzyData({ payload }, { call, put }) {
            let result = yield call(getFuzzyData, payload);
            if (result.statusCode === 200) {
                yield put({
                    type: 'save',
                    payload: { FuzzyData: result.data }
                })
            }
        },
        * getknowledges({ payload }, { call, put }) {
            let result = yield call(getknowledge);
            if (result.statusCode === 200) {
                yield put({
                    type: 'save',
                    payload: { knowledgeData: result.data[0], unm: result.data[1] }
                })
            }
        },
        * getknowledgedetailsdata({ payload }, { call, put }) {
            let result = yield call(getknowledgedetailsdata, payload);
            if (result.statusCode === 200) {
                yield put({
                    type: 'save',
                    payload: { knowledgedetails: result.data.children }
                })
            }
        },
        * getknowledgedetailsTowdata({ payload }, { call, put }) {
            let result = yield call(getknowledgedetailsTowdata, payload);
            if (result.statusCode === 200) {
                yield put({
                    type: 'save',
                    payload: { knowledgedetailsTowdata: result.data }
                })
            }
        },
        * getknowledgedetailsDataONE({ payload }, { call, put }) {
            let result = yield call(getknowledgedetailsDataONE, payload);
            if (result.statusCode === 200) {
                yield put({
                    type: 'save',
                    payload: { knowledgedetailsDataONE: result.data }
                })
            }
        },

    },
    reducers: {
        save(state, action) {
            return {
                ...state,
                ...action.payload,
            };
        },
    }
};

export default IndexModel;
