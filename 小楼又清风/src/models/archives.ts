import { getArchivesList, getArticleDetail, getArticleDetailRecommend } from "@/services"
import { RootObject, ClassifiCation, IArchivesItem, IArticleDetail, IDetailRecommend } from "@/types"//类型

// import { RootObject, ClassifiCation, IDetailRecommend } from '@/types/RecommendedReading';
import { Effect, ImmerReducer, Reducer, Subscription } from 'umi';

export interface ArchivesModelState {
    archivesList: IArchivesItem;//归档列表数据
    ArcRecommend: RootObject[];//推荐阅读
    ArcCategory: ClassifiCation[];//文章分类
    articleDetail: Partial<IArticleDetail>;
    articleDetailRecommend: IDetailRecommend[];//详情推荐阅读
}

export interface IndexModelType {
    namespace: 'archives';
    state: ArchivesModelState;
    effects: {
        getArchivesLists: Effect;
        getArticleDetail: Effect;
        getArticleDetailRecommend: Effect;

    };
    reducers: {
        save: Reducer<ArchivesModelState>;
    };
}

const IndexModel: IndexModelType = {
    namespace: 'archives',

    state: {
        archivesList: {},
        ArcRecommend: [],
        ArcCategory: [],
        articleDetail: {},//详情
        articleDetailRecommend: [],//详情右上角推荐阅读
    },

    effects: {
        //归档数据
        *getArchivesLists({ payload }, { call, put }) {
            let result = yield call(getArchivesList)
            if (result.statusCode === 200) {
                yield put({
                    type: "save",
                    payload: { archivesList: result.data }
                })
            }
        },
        //归档跳详情
        *getArticleDetail({ payload }, { call, put }) {
            let result = yield call(getArticleDetail, payload);
            if (result.statusCode === 200) {
                yield put({
                    type: 'save',
                    payload: { articleDetail: result.data }
                })
            }
        },
        //文章详情右上角推荐
        *getArticleDetailRecommend({ payload }, { call, put }) {
            let result = yield call(getArticleDetailRecommend, payload);
            if (result.statusCode === 200) {
                yield put({
                    type: 'save',
                    payload: { articleDetailRecommend: result.data }
                })
            }
        },

    },
    reducers: {
        save(state, action) {
            return {
                ...state,
                ...action.payload,
            };
        }
    }
};

export default IndexModel;
