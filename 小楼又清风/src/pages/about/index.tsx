import React, { useEffect, useState } from 'react'
import { RouteComponentProps } from 'react-router-dom'
import { useDispatch, useIntl, useSelector } from 'umi';
import { IRootState, ModelsItem, ArticleItem } from '@/types'
import style from './index.less'
import Cls from 'classnames'
import MessageBoard from '@/components/MessageBoard'
import CommentItem from '@/components/CommentItem'
import Pagina from '@/components/Pagina'
import ArticleList from '@/components/ArticleList'
import ShareMask from '@/components/ShareMask'
import ShowMask from '@/components/ShareMaskcopy'
interface Props {
    commentList: Array<ModelsItem>
    commentTotal: number,
    page: number
}
const about: React.FC<Props & RouteComponentProps> = (props) => {
    const intl = useIntl();
    let dispatch = useDispatch()
    let [page, setPage] = useState<number>(1)
    // let [maskFlag, setMaskFlag] = useState<boolean>(false)
    // let [itemShare, setItemShare] = useState({})
    let { view, commentList, commentTotal, getAbout } = useSelector((state: IRootState) => state.comments);
    let { recommend } = useSelector((state: IRootState) => state.article);
    // 获取数据
    useEffect(() => {
        dispatch({
            type: 'article/getRecommend',
        })
        dispatch({
            type: 'comments/getAbout',
        })
    }, [])
    useEffect(() => {
        dispatch({
            type: 'comments/getView',
            payload: getAbout.id
        })
    }, [getAbout])
    // 页数改变请求数据
    useEffect(() => {
        dispatch({
            type: 'comments/getCommentList',
            payload: {
                page,
                id: getAbout && getAbout.id
            }
        })
    }, [page, getAbout])
    // 改变数据
    let handleCurrent = (page: number) => {
        setPage(page)
    }
    let showMask = (item: ArticleItem) => {
        ShowMask(item)
        document.body.style.overflow = 'hidden';
        document.body.style.width = 'calc(100% - 4px)'
    }
    return (
        <div className={style.about}>
            {/* 头部 */}
            <header className={style.header}>
                <div className={Cls(style.banner,'container')}>
                    <div>
                        <img src={view.cover} alt="文章封面" />
                    </div>
                </div>
                <div>
                    <div dangerouslySetInnerHTML={{ __html: view.html }} className={style.title}>

                    </div>
                </div>
            </header>
            <main>
                <div className={style.main}>
                    <p className={style.comment}>{intl.formatMessage({id:"menu.comment"})}</p>
                    <MessageBoard id={getAbout.id} path={getAbout.path} />
                    {/* 评论区域 */}
                    <div>
                        {commentList && <CommentItem list={commentList} id={getAbout.id} path={getAbout.path} />}
                    </div>
                    {/* 分页区域 */}
                    <Pagina
                        total={commentTotal}
                        handleCurrent={handleCurrent}
                        page={page}
                    />

                </div>
                <div className={style.footer}>
                    <p className={style.recommend}>{intl.formatMessage({id:"menu.recommendedReadings"})}</p>
                    <div className={style.articleList}>
                        <ArticleList ArticleList={recommend} showMask={showMask} />
                    </div>
                </div>
            </main>
        </div>
    )
}

export default about
