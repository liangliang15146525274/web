import { IRootState } from '@/types';
import { useEffect, useState } from 'react';
import { useDispatch, useSelector, NavLink } from 'umi';
import { RouteComponentProps, Redirect } from 'react-router-dom'
import style from './index.less';
import Cln from 'classnames'
import ScrollAdd from '@/components/ScrollAdd'
import RecommendedReading from '@/components/RecommendedReading'
import ClassifiCation from '@/components/ClassifiCation'
interface Props extends RouteComponentProps {

}

const tag: React.FC<Props> = (props) => {
    let [Ind, setInd] = useState(0)
    const { recommend, ategoryTitle, TagData, TagNum } = useSelector((state: IRootState) => state.article);
    useEffect(() => {
        let name = props.location.pathname.substr(4)
        let num = TagData.findIndex(item => item.value === name) && 0
        setInd(num)
    }, [TagData])
    return (
        <div className={Cln(style.tag, style.index)}>
            <div className={Cln(style.container, 'container')}>
                <div className={Cln(style.index_left, style.tag_left, 'index_left')}>
                    <div className={style.category_title}>
                        <p>与<span>{TagData[Ind] && TagData[Ind].label}</span>标签有关的文章</p>
                        <p className={style.Count}>共搜索到<span className={style.num}>{TagData[Ind] && TagData[Ind].articleCount}</span>篇</p>
                    </div>
                    <div className={style.list}>
                        <h3 className={style.list_h3}>文章标签</h3>
                        <div className={style.list_title}>
                            {props.location.pathname === '/tag' ? <Redirect exact from='/tag' to='/tag/git' /> : ''}
                            {
                                TagData.map((item, index) => {
                                    return <NavLink activeClassName={style.active} key={item.id} to={`/tag/${item.value}`} onClick={() => {
                                        setInd(index)
                                    }}>
                                        <span>{item.label} [{item.articleCount}]</span>
                                    </NavLink>
                                })
                            }
                        </div>
                        <div>
                            {props.children}
                        </div>
                    </div>
                </div>
                <div className={Cln(style.index_right, 'index_right')}>
                    <div className='sticky'>
                        {/* 文章推荐 */}
                        <RecommendedReading infor={recommend} />
                        {/* 文章分类 */}
                        <ClassifiCation infor={ategoryTitle} />
                    </div>
                </div>
            </div>
        </div>
    )
}

export default tag
