import { IRootState } from '@/types';
import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'umi';
import style from './index.less';
import ScrollAdd from '@/components/ScrollAdd'

interface Props {

}

const docker: React.FC = (props) => {
  const dispatch = useDispatch()
  const [page, setpage] = useState(1)
  const { TagClassify, TagClassifyNum } = useSelector((state: IRootState) => state.article);

  useEffect(() => {
    dispatch({
      type: 'article/getTagClassify',
      Classify: 'docker',
      payload: page
    })
  }, [page])

  const fetchMoreData = () => {
    setpage(page => page + 1)
  }
  return (
    <ScrollAdd ArticleList={TagClassify} ArticleNum={TagClassifyNum} page={page} fetchMoreData={fetchMoreData} />
  )
}
export default docker
