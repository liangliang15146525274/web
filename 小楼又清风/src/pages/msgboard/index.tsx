import React, { useState, useEffect } from 'react'
import style from './index.less'
import { RouteComponentProps } from 'react-router-dom'
import { useDispatch, useIntl, useSelector } from 'umi';
import { IRootState, ModelsItem, ArticleItem } from '@/types'
//引入组件
import MessageBoard from '@/components/MessageBoard'
import CommentItem from '@/components/CommentItem'
import Pagina from '@/components/Pagina'
import ArticleList from '@/components/ArticleList'
import ShowMask from '@/components/ShareMaskcopy';
interface Props {
    commentList: Array<ModelsItem>
    commentTotal: number,
    page: number
}
const msgboard: React.FC<Props & RouteComponentProps> = (props) => {
    const intl = useIntl()
    let dispatch = useDispatch()
    let [page, setPage] = useState<number>(1)
    let { commentList, commentTotal, getMsgboard } = useSelector((state: IRootState) => state.comments);
    let { recommend } = useSelector((state: IRootState) => state.article);
    let [maskFlag, setMaskFlag] = useState(false)
    let [itemShare, setItemShare] = useState({})

    // 获取数据
    useEffect(() => {
        dispatch({
            type: 'article/getRecommend',
        })
        dispatch({
            type: 'comments/getMsgboard',
        })
    }, [])
    //留言板评论
    useEffect(() => {
        dispatch({
            type: 'comments/getCommentList',
            payload: {
                page,
                id: getMsgboard.id
            }
        })
    }, [page, getMsgboard])
    // 点击分页按钮切换数据
    let handleCurrent = (page: number) => {
        setPage(page)
    }

    let showMask = (item: ArticleItem) => {
        ShowMask(item)
        document.body.style.overflow = 'hidden';
        document.body.style.width = 'calc(100% - 4px)'
    }
    return (
        <div className={style.box}>
            <div className={style.context}>
                {/* 头部 */}
                <div className={style.container}>
                    <div className={style.markdown}>
                        <h2>留言板</h2>
                        <p>
                            <strong>
                                请勿灌水 🤖
                            </strong>
                        </p>
                    </div>
                </div>
            </div>
            <div className={style.main}>
                <div className={style.ping}>
                    {intl.formatMessage({id:"menu.comment"})}
                </div>
                {/* 留言板 */}
                <MessageBoard id={getMsgboard.id} path={getMsgboard.path} />
                {/* 评论区域 */}
                <div>
                    {commentList && <CommentItem list={commentList} id={getMsgboard.id} path={getMsgboard.path} />}
                </div>
                {/* 分页区域 */}
                <Pagina
                    total={commentTotal}
                    handleCurrent={handleCurrent}
                    page={page}
                />
                {/* 阅读区域 */}
                <div className={style.read}></div>
                <div className={style.recommend}>
                    {intl.formatMessage({id:"menu.recommendedReadings"})}
                </div>
                <div className={style.articleList}>
                    <ArticleList ArticleList={recommend} showMask={showMask} />
                </div>
            </div>

        </div>
    )
}

export default msgboard
