import { IRootState } from '@/types';
import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'umi';
import ScrollAdd from '@/components/ScrollAdd'

interface Props {

}

const linux: React.FC = (props) => {
    const dispatch = useDispatch()
    const [page, setpage] = useState(1)
    const { ArticleClassify, ArticleClassifyNum } = useSelector((state: IRootState) => state.article);

    useEffect(() => {
        dispatch({
            type: 'article/getArticleClassify',
            Classify: 'linux',
            payload: page
        })
    }, [page])

    const fetchMoreData = () => {
        setpage(page => page + 1)
    }
    return (
        <ScrollAdd ArticleList={ArticleClassify} ArticleNum={ArticleClassifyNum} page={page} fetchMoreData={fetchMoreData} />
    )
}

export default linux
