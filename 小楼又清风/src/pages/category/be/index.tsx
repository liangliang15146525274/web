import { IRootState } from '@/types';
import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'umi';
import style from './index.less';
import ScrollAdd from '@/components/ScrollAdd'

interface Props {

}

const be: React.FC = (props) => {
  const dispatch = useDispatch()
  const [page, setpage] = useState(1)
  const { ArticleClassify, ArticleClassifyNum } = useSelector((state: IRootState) => state.article);

  useEffect(() => {
    dispatch({
      type: 'article/getArticleClassify',
      Classify: 'be',
      payload: page
    })
  }, [page])

  const fetchMoreData = () => {
    setpage(page => page + 1)
  }
  return (
    <ScrollAdd ArticleList={ArticleClassify} ArticleNum={ArticleClassifyNum} page={page} fetchMoreData={fetchMoreData} />
  )
}
export default be
