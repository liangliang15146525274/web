import { IRootState } from '@/types';
import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'umi';
import ScrollAdd from '@/components/ScrollAdd'

interface Props {

}

const reading: React.FC = (props) => {
    const dispatch = useDispatch()
    const [page, setpage] = useState(1)
    const { ArticleClassify, ArticleClassifyNum } = useSelector((state: IRootState) => state.article);

    console.log(ArticleClassify, "bb",ArticleClassifyNum ,"aa");
    
    useEffect(() => {
        dispatch({
            type: 'article/getArticleClassify',
            Classify: 'reading',
            payload: page
        })
    }, [page])

    const fetchMoreData = () => {
        setpage(page => page + 1)
    }
    return (
        <ScrollAdd ArticleList={ArticleClassify} ArticleNum={ArticleClassifyNum} page={page} fetchMoreData={fetchMoreData} />
    )
}

export default reading
