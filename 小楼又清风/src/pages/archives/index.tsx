import React,{ useState, useRef,useEffect} from "react" 
import { IRootState, July, June } from "@/types"
import { useDispatch, useSelector, useIntl } from "@/.umi/core/umiExports"
import Cls from 'classnames'
import style from './index.less' 
import { RouteComponentProps ,NavLink} from "react-router-dom"
import RecommendedReading from '@/components/RecommendedReading'
import ClassifiCation from '@/components/ClassifiCation'
import moment from "moment" 
const archives: React.FC<RouteComponentProps> = (props) => {
    const intl = useIntl();
    const dispatch = useDispatch() 
    const [num,setNum] = useState (0)
    const archives_right: any = useRef()
    const { archivesList } = useSelector((state: IRootState) => state.archives)
    const { recommend, ategoryTitle } = useSelector((state: IRootState) => state.article);
    useEffect(() => {
        dispatch({
            type: "archives/getArchivesLists"
        }) 
    }, []) 
    useEffect(() => {
        let total:any[] = [];
        let total_title = 0;
        Object.values(archivesList).forEach(item => {
            Object.values(item).forEach(ite => {
                total.push(ite.length)
            })
            total_title = total.reduce((prev, next) => {
                return prev += next
            })
        });
        setNum(total_title) 
    }, [archivesList]);
   

    return (
        <div className={Cls(style.archives,'container')}>
            <div className={Cls(style.archives_left,'index_left')}>
                <div className={style.archives_left_content}>
                    <div className={style.header}>
                        <p><span>{intl.formatMessage({id:'menu.archives'})}</span> </p>
                        <p>{intl.formatMessage({id:'menu.total'})} <span>  {num}  </span> {intl.formatMessage({id:'menu.count'})}</p>
                    </div>
                    {
                        Object.keys(archivesList).sort((a, b) => Number(b) - Number(a)).map((ite1, i) => {
                            return <div className={style.big} key={ite1}>
                                <h2 key={i}>{ite1}</h2>
                                {Object.keys(archivesList[ite1]).map((ite2, i2) => {
                                    return <div key={ite2} className={style.match_content}>
                                        <h3>{ite2}</h3>
                                        <ul>
                                            {Object.values(archivesList[ite1][ite2] as July).map((ite3, i3) => {
                                                return <li key={ite3.id} style={{ opacity: 1, height: "48px", transform: "none" }} >
                                                    <NavLink className={style.li_a} to={`/detail/${ite3.id}`}>
                                                        <span className={style.time_time}>{moment(ite3.createAt).format("MM-DD")}</span>
                                                        <span className={style.item_title}>{ite3.title}</span>
                                                    </NavLink>
                                                </li>
                                            })}
                                        </ul>
                                    </div>
                                })} 
                            </div>
                        })
                    }
                </div>
            </div>

            <div className={Cls(style.archives_right,'index_right')}>
                <div className={Cls(style.sticky,'sticky')}>
                    <div className={style.sticky_top}>
                        {
                            recommend && <RecommendedReading infor={recommend} />
                        }
                    </div>
                    <div className={style.sticky_bottom}>
                        {ategoryTitle && <ClassifiCation infor={ategoryTitle} />}
                    </div>
                </div>
            </div>
        </div>
    )
}

export default archives;
