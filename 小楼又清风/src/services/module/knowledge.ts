import { request } from 'umi';

// 获取页面的数据
export function getknowledge(page: number, pageSize = 12, status = 'publish') {
    return request('/api/knowledge', {
        params: {
            page,
            pageSize,
            status,
        }
    })
}

// 获取文章分类的数据
export function getcategory() {
    return request('/api/category')
}

export function getknowledgedetailsdata(id: string) {
    return request(`/api/knowledge/${id}`)
}

// 第二层详情
export function getknowledgedetailsTowdata(id: string) {
    return request(`/api/knowledge/${id}/views`, {
        method: "POST"
    })
}

// 第二层详情右侧
export function getknowledgedetailsDataONE(id: string) {
    return request(`/api/knowledge/${id}/views`, {
        method: "POST"
    })
}


//
export function getFuzzyData(val: string) {
    return request(`/api/search/article?keyword=${val}`,)
}
