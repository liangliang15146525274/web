//评论列表
import { useEffect, useRef, useState } from 'react'
import { ModelsItem, ChildItem } from '@/types'
import monment from '@/utils/moment'
import style from './index.less'
import InpTextArea from '@/components/InpTextArea'
import classnames from 'classnames'

interface Props {
    // item: ModelsItem
    list: ModelsItem[]
    id: string
    path?: string
}
const CommentItem: React.FC<Props> = (props) => {
    const { list, id, path } = props
    //控制显示回复框
    // const textareaList = useRef<Array<HTMLDivElement>>([]);
    const [lists, setLists] = useState<Array<string>>([])
    //判断点击回复，控制回复框显示隐藏
    const handleShow = (item: ModelsItem | ChildItem) => {
        let index = lists.findIndex(val => val === item.id);
        if (index === -1) {
            setLists([...lists, item.id])
        } else {
            lists.splice(index, 1);
            setLists([...lists]);
        }
    }
    //点击收起隐藏回复框
    const handleBackUp = (id: string) => {
        let index = lists.findIndex(val => val === id);
        lists.splice(index, 1);
        setLists([...lists]);
    }
    return <div className={style.comment}>
        {
            // 一级评论
            list && list.map((item) => {
                return <div className={style.item} key={item.id}>
                    <header>
                        <span className={style.avatar} style={{ background: '#09f'}}>
                            <span className={style.avataritem}>
                                {item.name.slice(0, 1).toLocaleUpperCase()}
                            </span>
                        </span>
                        <span className={style.name}>
                            <strong>{item.name}</strong>
                        </span>
                    </header>
                    <main>
                        <div>
                            <p dangerouslySetInnerHTML={{ __html: item.content }}></p>
                        </div>
                    </main>
                    <footer className={style.footer}>
                        <div className={style.times}>
                            <span>{item.userAgent&&item.userAgent+' · '}</span>
                            <time>{monment(item.createAt).toNow()}</time>
                            <span className={style.reply} onClick={() => handleShow(item)}>
                                <span role="img" aria-label="message" className={style.anticon}>
                                    <svg viewBox="64 64 896 896" focusable="false" data-icon="message" width="1em" height="1em" fill="currentColor" aria-hidden="true">
                                        <path d="M464 512a48 48 0 1096 0 48 48 0 10-96 0zm200 0a48 48 0 1096 0 48 48 0 10-96 0zm-400 0a48 48 0 1096 0 48 48 0 10-96 0zm661.2-173.6c-22.6-53.7-55-101.9-96.3-143.3a444.35 444.35 0 00-143.3-96.3C630.6 75.7 572.2 64 512 64h-2c-60.6.3-119.3 12.3-174.5 35.9a445.35 445.35 0 00-142 96.5c-40.9 41.3-73 89.3-95.2 142.8-23 55.4-34.6 114.3-34.3 174.9A449.4 449.4 0 00112 714v152a46 46 0 0046 46h152.1A449.4 449.4 0 00510 960h2.1c59.9 0 118-11.6 172.7-34.3a444.48 444.48 0 00142.8-95.2c41.3-40.9 73.8-88.7 96.5-142 23.6-55.2 35.6-113.9 35.9-174.5.3-60.9-11.5-120-34.8-175.6zm-151.1 438C704 845.8 611 884 512 884h-1.7c-60.3-.3-120.2-15.3-173.1-43.5l-8.4-4.5H188V695.2l-4.5-8.4C155.3 633.9 140.3 574 140 513.7c-.4-99.7 37.7-193.3 107.6-263.8 69.8-70.5 163.1-109.5 262.8-109.9h1.7c50 0 98.5 9.7 144.2 28.9 44.6 18.7 84.6 45.6 119 80 34.3 34.3 61.3 74.4 80 119 19.4 46.2 29.1 95.2 28.9 145.8-.6 99.6-39.7 192.9-110.1 262.7z"
                                            fill="#00000059"
                                        >
                                        </path>
                                    </svg>
                                    &nbsp;回复
                                </span>
                            </span>
                        </div>
                        <div className={classnames(style.box, lists.indexOf(item.id) == -1 ? '' : style.inpshow)}>
                            {
                                lists?.map(val => {
                                    if (val === item.id) {
                                        return (
                                            <InpTextArea
                                                handleBackUp={handleBackUp}
                                                key={val}
                                                item={item}
                                                id={id}
                                                path={path!}
                                            />
                                        )
                                    }
                                })
                            }
                        </div>
                        <div>
                            {/* 二级评论 */}
                            {item.children && item.children.map((items,) => {
                                return <div className={style.item} key={items.id}>
                                    <header>
                                        <span className={style.avatar} style={{ background: '#09f'}}>
                                            <span className={style.avataritem}>
                                                {items.name.slice(0, 1).toLocaleUpperCase()}
                                            </span>
                                        </span>
                                        <span className={style.name}>
                                            <strong>{items.name} 回复 {items.replyUserName}</strong>
                                        </span>
                                    </header>

                                    <main>
                                        <div>
                                            <p dangerouslySetInnerHTML={{ __html: items.content }}></p>
                                        </div>
                                    </main>
                                    <footer className={style.footer}>
                                        <div className={style.times}>
                                            <span>{item.userAgent&&item.userAgent+' · '}</span>
                                            <time>{monment(items.createAt).toNow()}</time>
                                            <span className={style.reply}>
                                                <span role="img" aria-label="message" className={style.anticon} onClick={() => handleShow(items)}>
                                                    <svg viewBox="64 64 896 896" focusable="false" data-icon="message" width="1em" height="1em" fill="currentColor" aria-hidden="true">
                                                        <path d="M464 512a48 48 0 1096 0 48 48 0 10-96 0zm200 0a48 48 0 1096 0 48 48 0 10-96 0zm-400 0a48 48 0 1096 0 48 48 0 10-96 0zm661.2-173.6c-22.6-53.7-55-101.9-96.3-143.3a444.35 444.35 0 00-143.3-96.3C630.6 75.7 572.2 64 512 64h-2c-60.6.3-119.3 12.3-174.5 35.9a445.35 445.35 0 00-142 96.5c-40.9 41.3-73 89.3-95.2 142.8-23 55.4-34.6 114.3-34.3 174.9A449.4 449.4 0 00112 714v152a46 46 0 0046 46h152.1A449.4 449.4 0 00510 960h2.1c59.9 0 118-11.6 172.7-34.3a444.48 444.48 0 00142.8-95.2c41.3-40.9 73.8-88.7 96.5-142 23.6-55.2 35.6-113.9 35.9-174.5.3-60.9-11.5-120-34.8-175.6zm-151.1 438C704 845.8 611 884 512 884h-1.7c-60.3-.3-120.2-15.3-173.1-43.5l-8.4-4.5H188V695.2l-4.5-8.4C155.3 633.9 140.3 574 140 513.7c-.4-99.7 37.7-193.3 107.6-263.8 69.8-70.5 163.1-109.5 262.8-109.9h1.7c50 0 98.5 9.7 144.2 28.9 44.6 18.7 84.6 45.6 119 80 34.3 34.3 61.3 74.4 80 119 19.4 46.2 29.1 95.2 28.9 145.8-.6 99.6-39.7 192.9-110.1 262.7z"
                                                            fill="#00000059"
                                                        >
                                                        </path>
                                                    </svg>
                                                    &nbsp;回复
                                                </span>
                                            </span>
                                        </div>
                                        <div className={classnames(style.box, lists.indexOf(items.id) == -1 ? '' : style.inpshow)}>
                                            {
                                                lists?.map(val => {
                                                    if (val === items.id) {
                                                        return (
                                                            <InpTextArea
                                                                handleBackUp={handleBackUp}
                                                                key={val}
                                                                item={items}
                                                                id={id}
                                                                path={path!}
                                                            />
                                                        )
                                                    }
                                                })
                                            }
                                        </div>
                                    </footer>
                                </div>
                            })}
                        </div>
                    </footer>
                </div>
            })
        }
    </div>
}

export default CommentItem
