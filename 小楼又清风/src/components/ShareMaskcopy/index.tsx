import React, { useEffect, useState } from 'react'
import style from './index.less'
import { ArticleItem, IArticleDetail, MaskItem } from '@/types'
import ReactDOM from 'react-dom';
import QRCode from 'qrcode'
import { genePoster } from '@/services';
interface Props {
    item: MaskItem | IArticleDetail | ArticleItem,
}

export const ShareMask: React.FC<Props> = (props) => {
    const [qrSrc, setQrSrc] = useState('');
    const [downloadSrc, setDownloadSrc] = useState('');
    function cancelClick() {
        document.body.style.overflow = 'auto';
        document.body.style.width = 'calc(100%)'
        document.querySelector('#mask')?.remove()
        // ReactDOM.unmountComponentAtNode(document.querySelector('#mask')!);
    }
    // 生成海报
    useEffect(() => {
        if (props.item && qrSrc) {
            console.log(document.querySelector("#mask")!.innerHTML!);
            genePoster({
                height: 861,
                html: document.querySelector("#mask")!.innerHTML!,
                name: props.item.title,
                pageUrl: ``,
                width: 391
            }).then(res => {
                setDownloadSrc(res.data.url);
            })
        }
    }, [props.item, qrSrc]);

    const url = `https://blog.wipi.tech/knowledge/${props.item.id}`;
    useEffect(() => {
        if (props.item) {
            QRCode.toDataURL(url)
                .then((url: string) => {
                    setQrSrc(url)
                })
                .catch(err => {
                    console.error(err)
                })
        }
    }, [props.item]);
    return (
        <>
            {
                <div className={style.ShareMask}>
                    <div className={style.mask_con}>
                        <div className={style.mask_content}>
                            <div className={style.mask_herder}>
                                <div> <h3> 分享海报 </h3> </div>
                                <div onClick={() => cancelClick()} style={{ cursor: "pointer" }}>X</div>
                            </div>
                            <div className={style.mask_contents}>
                                <p> <img src={props.item.cover} alt="" /> </p>
                                <h4 className={style.mask_title}> {props.item.title} </h4>
                                <p> {props.item.summary}</p>
                                <div className={style.mask_qrCode}>
                                    <div className={style.mask_qrCode_img}> <img src={qrSrc} alt="加载错误.." />  </div>
                                    <div >
                                        <p>识别二维码查看文章</p>
                                        <p> 原文分享自 <span>小楼又清风</span></p>
                                    </div>
                                </div>
                            </div>
                            <div className={style.mask_bottom}>
                                <p> <button onClick={() => cancelClick()}>关闭</button><button onClick={() => window.location.href = downloadSrc}>下载</button> </p>
                            </div>
                        </div>
                    </div>
                </div>
            }
        </>
    )
}

export default function ShowMask(item: MaskItem) {
    let shareDialog = document.querySelector('#mask');
    if (!shareDialog) {
        shareDialog = document.createElement('div');
        shareDialog.id = 'mask';
        document.body.appendChild(shareDialog);
    }
    ReactDOM.render(<ShareMask item={item} />, shareDialog);
}
