import { createRef, useEffect, useRef, useState } from 'react';
import { NavLink, history, useDispatch, useSelector, useIntl, setLocale } from 'umi';
import { Menu, Dropdown, Button, Space } from 'antd'
import { IRootState } from '@/types';
import Cls from 'classnames'
import './index.less'


interface Props {
    setSearchFlags(flag: boolean): void
}


const Header: React.FC<Props> = (props) => {
    const intl = useIntl();
    const [NavShow, setNavShow] = useState(false)
    const dispatch = useDispatch()
    let active: any = createRef()
    const { language } = useSelector((state: IRootState) => {
        return {
            language: state.language
        }
    })

    useEffect(() => {
        setLocale(language.locale, false);
    }, [language.locale]);
    //设置语言
    function changeLanguage(locale: string) {
        dispatch({
            type: 'language/save',
            payload: { locale }
        })
    }
    //中英文切换
    const menu = (
        <Menu>
            {language.locales.map((item, index) => {
                return <Menu.Item key={index}>
                    <span onClick={() => changeLanguage(item.value)}>
                        {intl.formatMessage({ id: item.label, defaultMessage: '' })}
                    </span>
                </Menu.Item>
            })}
        </Menu>
    );
    const menus = [{
        title: 'menu.article',
        link: '/',
        exact: true
    }, {
        title: 'menu.archives',
        link: '/archives'
    }, {
        title: 'menu.knowledge',
        link: '/knowledge'
    }, {
        title: 'menu.msgboard',
        link: '/msgboard'
    }, {
        title: 'menu.about',
        link: '/about'
    },]

    useEffect(() => {
        dispatch({
            type: 'article/getRecommend'
        })
        dispatch({
            type: 'article/getcAtegoryTitle',
        })
        dispatch({
            type: 'article/getTagData',
        })
    }, [])
    function setactive() {
        active.current.classList.toggle("active")
        document.body.classList.toggle("dark")
    }
    return <header className='layouts_header'>
        <div className='container'>
            <div className='header_logo' onClick={() => {
                history.push('/')
            }}>
                <img src="https://wipi.oss-cn-shanghai.aliyuncs.com/2021-02-20/wipi-logo.png" alt="" />
            </div>
            <div className="nav_but">
                <div className={Cls('content', NavShow ? "active" : '')} onClick={((e) => {
                    setNavShow(!NavShow)
                })}>
                    <div></div>
                    <div></div>
                    <div></div>
                </div>
            </div>
            <nav className={Cls('header_nav', NavShow ? "active" : '')} onClick={() => {
                window.innerWidth < 768 ? setNavShow(!NavShow) : ''
            }}>
                {
                    menus.map((item, index) => {
                        return <NavLink to={item.link}
                            key={index}
                            exact={item.exact}
                        >
                            {intl.formatMessage({ id: item.title })}
                        </NavLink>
                    })
                }
                <div className='header_right'>
                    <Space direction="vertical">
                        <Dropdown overlay={menu} placement="bottomCenter">
                            <svg viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" width="1em" height="1em"><path d="M547.797333 638.208l-104.405333-103.168 1.237333-1.28a720.170667 720.170667 0 0 0 152.490667-268.373333h120.448V183.082667h-287.744V100.906667H347.605333v82.218666H59.818667V265.386667h459.178666a648.234667 648.234667 0 0 1-130.304 219.946666 643.242667 643.242667 0 0 1-94.976-137.728H211.541333a722.048 722.048 0 0 0 122.453334 187.434667l-209.194667 206.378667 58.368 58.368 205.525333-205.525334 127.872 127.829334 31.232-83.84m231.424-208.426667h-82.218666l-184.96 493.312h82.218666l46.037334-123.306667h195.242666l46.464 123.306667h82.218667l-185.002667-493.312m-107.690666 287.744l66.56-178.005333 66.602666 178.005333z" fill="currentColor"></path></svg>
                        </Dropdown>
                    </Space>
                    <div className='two'>
                        <div className="sun" ref={active} onClick={() => setactive()}>
                            <span></span>
                            <span></span>
                            <small className="sunItem"></small>
                            <small className="sunItem"></small>
                            <small className="sunItem"></small>
                            <small className="sunItem"></small>
                            <small className="sunItem"></small>
                            <small className="sunItem"></small>
                        </div>
                    </div>
                    <div onClick={() => {
                        props.setSearchFlags(true)
                    }}><svg viewBox="64 64 896 896" focusable="false" data-icon="search" width="1em" height="1em" fill="currentColor" aria-hidden="true"><path d="M909.6 854.5L649.9 594.8C690.2 542.7 712 479 712 412c0-80.2-31.3-155.4-87.9-212.1-56.6-56.7-132-87.9-212.1-87.9s-155.5 31.3-212.1 87.9C143.2 256.5 112 331.8 112 412c0 80.1 31.3 155.5 87.9 212.1C256.5 680.8 331.8 712 412 712c67 0 130.6-21.8 182.7-62l259.7 259.6a8.2 8.2 0 0011.6 0l43.6-43.5a8.2 8.2 0 000-11.6zM570.4 570.4C528 612.7 471.8 636 412 636s-116-23.3-158.4-65.6C211.3 528 188 471.8 188 412s23.3-116.1 65.6-158.4C296 211.3 352.2 188 412 188s116.1 23.2 158.4 65.6S636 352.2 636 412s-23.3 116.1-65.6 158.4z"></path></svg></div>
                </div>
            </nav>

        </div>

    </header >
}

export default Header;
