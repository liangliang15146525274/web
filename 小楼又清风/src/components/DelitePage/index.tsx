import React, { ReactElement } from 'react'
import { RouteComponentProps, withRouter } from 'react-router-dom'
import style from '@/pages/KnowDetailsTwo/index.less'
import { Knowledgedetails, MaskItem } from '@/types'
interface Props extends RouteComponentProps {
    knowledgedetails: Knowledgedetails[],
    id: string,
    item: MaskItem
}

let DelitePage: React.FC<Props> = function (props) {
    let { knowledgedetails, id } = props
    return (
        <>
            {
                knowledgedetails && knowledgedetails.findIndex(item => item.id === id) === 0 ? <div className={style.delitePageOwn}
                    onClick={() => {
                        props.history.push({
                            pathname: `/KnowDetailsTwo/${knowledgedetails[knowledgedetails.findIndex(item => item.id === id) + 1].id}`,
                            state: {
                                title: knowledgedetails[knowledgedetails.findIndex(item => item.id === id) + 1].title,
                                parentNodeId: knowledgedetails[knowledgedetails.findIndex(item => item.id === id) + 1].parentId,
                                item: props.item
                            }
                        })
                    }}>
                    {knowledgedetails[knowledgedetails.findIndex(item => item.id === id) + 1].title}&gt;
                </div> : ''
            }
            {
                knowledgedetails && knowledgedetails.findIndex(item => item.id === id) === knowledgedetails.length - 1 ? <div
                    onClick={() => {
                        props.history.push({
                            pathname: `/KnowDetailsTwo/${knowledgedetails[knowledgedetails.findIndex(item => item.id === id) - 1].id}`,
                            state: {
                                title: knowledgedetails[knowledgedetails.findIndex(item => item.id === id) - 1].title,
                                parentNodeId: knowledgedetails[knowledgedetails.findIndex(item => item.id === id) - 1].parentId,
                                item: props.item
                            }
                        })
                    }}
                    className={style.delitePageTwo}
                >
                    &lt; {knowledgedetails[knowledgedetails.findIndex(item => item.id === id) - 1] && knowledgedetails[knowledgedetails.findIndex(item => item.id === id) - 1].title}
                </div> : ''
            }
            {
                knowledgedetails && knowledgedetails.findIndex(item => item.id === id) !== 0 && knowledgedetails.findIndex(item => item.id === id) !== knowledgedetails.length - 1 ? <div className={style.delitePageSan}>
                    <div onClick={() => {
                        props.history.push({
                            pathname: `/KnowDetailsTwo/${knowledgedetails[knowledgedetails.findIndex(item => item.id === id) - 1].id}`,
                            state: {
                                title: knowledgedetails[knowledgedetails.findIndex(item => item.id === id) - 1].title,
                                parentNodeId: knowledgedetails[knowledgedetails.findIndex(item => item.id === id) - 1].parentId,
                                item: props.item
                            }
                        })
                    }} className={style.delitePageItem}>&lt;{knowledgedetails[knowledgedetails.findIndex(item => item.id === id) - 1] && knowledgedetails[knowledgedetails.findIndex(item => item.id === id) - 1].title}</div>
                    <div onClick={() => {
                        props.history.push({
                            pathname: `/KnowDetailsTwo/${knowledgedetails[knowledgedetails.findIndex(item => item.id === id) + 1].id}`,
                            state: {
                                title: knowledgedetails[knowledgedetails.findIndex(item => item.id === id) + 1].title,
                                parentNodeId: knowledgedetails[knowledgedetails.findIndex(item => item.id === id) + 1].parentId,
                                item: props.item
                            }
                        })
                    }} className={style.delitePageItem}>{knowledgedetails[knowledgedetails.findIndex(item => item.id === id) + 1].title}&gt;</div>
                </div> : ''
            }
        </>
    )
}


export default withRouter(DelitePage)
