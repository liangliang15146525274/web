import React from 'react'
import { IArticleItem } from '@/types'
import {RouteComponentProps, withRouter} from 'react-router-dom'
import { Carousel } from 'antd';
import style from './index.less'

interface Prop extends RouteComponentProps  {
    recommend:IArticleItem[]
}

let Carousels: React.FC<Prop> = function (props) {
    return (
        <div className={style.carousel}>
            <Carousel>
              {
                props.recommend.map(item => {
                  return item.isRecommended && <div className={style.carouselItem} key={item.id} onClick={()=> {
                    props.history.push(`/detail/${item.id}`)
                }} >
                    <div className={style.bg} style={{ backgroundImage: `url(${item.cover})`}}></div>
                    <div className={style.mask}>
                      <div className={style.text}>
                        <h2>{item.title}</h2>
                        <p>{item.views} 次阅读</p>
                      </div>
                    </div>
                  </div>
                })
              }
            </Carousel>
          </div>
    )
}

export default withRouter(Carousels)
