import React, { useEffect, useState } from 'react'
import style from './index.less'
import { MaskItem } from '@/types'
import QRCode from 'qrcode'
interface Props {
    flag: boolean,
    item: MaskItem | any,
    setflag(flag: boolean): void
}

const ShareMasks: React.FC<Props> = (props) => {
    const [qrSrc, setQrSrc] = useState('');
    const [downloadSrc, setDownloadSrc] = useState('');
    const url = `https://blog.wipi.tech/knowledge/${props.item.id}`;
    useEffect(() => {
        if (props.item) {
            QRCode.toDataURL(url)
                .then((url: string) => {
                    setQrSrc(url)
                })
                .catch(err => {
                    console.error(err)
                })
        }
    }, [props.item]);

    return (
        <>
            {
                props.flag && <div className={style.ShareMask}>
                    <div className={style.mask_con}>
                        <div className={style.mask_content}>
                            <div className={style.mask_herder}>
                                <div> <h3> 分享海报 </h3> </div>
                                <div onClick={() => props.setflag(false)}>X</div>
                            </div>
                            <div className={style.mask_contents}>
                                <p> <img src={props.item.cover} alt="" /> </p>
                                <h4 className={style.mask_title}> {props.item.title} </h4>
                                <p> {props.item.summary}</p>
                                <div className={style.mask_qrCode}>
                                    <div className={style.mask_qrCode_img}> <img src={qrSrc} alt="加载错误.." />  </div>
                                    <div >
                                        <p>识别二维码查看文章</p>
                                        <p> 原文分享自 <span>小楼又清风</span></p>
                                    </div>
                                </div>
                            </div>
                            <div className={style.mask_bottom}>
                                <p> <button onClick={() => props.setflag(false)}>关闭</button><button onClick={() => console.log(23)}>下载</button> </p>
                            </div>
                        </div>
                    </div>
                </div>
            }
        </>
    )
}

export default ShareMasks
