import React from 'react'
import { RootObject } from '@/types/RecommendedReading'
import { withRouter } from 'react-router-dom'
import { RouteComponentProps } from "react-router-dom"
import style from './index.less'
import moment from "moment"
import { useIntl } from 'umi'
interface Prop {
    infor: RootObject[]
}

let RecommendedReading: React.FC<Prop & RouteComponentProps> = function (props) {
    const intl = useIntl();
    return (
        <div className={style.RecommendedReading}>
            <div className={style.RecommendedReading_top}> <span> <b>{intl.formatMessage({id:'menu.recommendedReadings'})}</b> </span> </div>
            <div >
                <ul className={style.content}>
                    {
                        props.infor.map((item, index) => {
                            return <li key={index} onClick={() => {
                                props.history.replace(`/detail/${item.id}`)
                            }}>
                                {item.title} <span>·</span> <span> {moment(item.createAt).toNow()} </span>
                            </li>
                        })
                    }
                </ul>

            </div>

        </div>
    )
}

export default withRouter(RecommendedReading)
