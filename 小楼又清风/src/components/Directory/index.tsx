import React, { useRef } from 'react'
import style from './index.less'
import "../../less/markdown.less"
import { useIntl } from 'umi';
interface Props {
    directoryList: any,
    del_scroll(index: number): void,
    ind: number
}

const Directory: React.FC<Props> = (props) => {
    const intl = useIntl();
    const directoryTitle = useRef(null)
    return (
        <div className={style.directory} >
            {
                props.directoryList.length > 0 && <header>{intl.formatMessage({id:'menu.toc'})}</header>
            }
            <main>
                <div className={style.directory_title} ref={directoryTitle}>
                    {
                        props.directoryList.map((item: any, index: number) => {
                            return <div key={item.id} style={{ opacity: "1", height: "32px" }}>
                                {
                                    item.level === "2" ?
                                        <div
                                            style={{ paddingLeft: "12px", cursor: "pointer" }}
                                            className={`${style.level2_title} ${props.ind === index ? style.light : ""}`}
                                            onClick={() => props.del_scroll(index)}
                                        >
                                            <li>{item.text}</li>
                                        </div> : ""
                                }
                                {
                                    item.level === "3" ?
                                        <div style={{ paddingLeft: "24px", cursor: "pointer" }}
                                            className={`${style.level3_title} ${props.ind === index ? style.light : ""}`}
                                            onClick={() => props.del_scroll(index)}>
                                            {item.text}
                                        </div>
                                        : ""
                                }
                                {
                                     item.level === "4" ?
                                     <div style={{ paddingLeft: "36px", cursor: "pointer" }}
                                         className={`${style.level4_title} ${props.ind === index ? style.light : ""}`}
                                         onClick={() => props.del_scroll(index)}>
                                         {item.text}
                                     </div>
                                     : ""
                                }
                            </div>
                        })
                    }
                </div>
            </main>
        </div>
    )
}

export default Directory;
