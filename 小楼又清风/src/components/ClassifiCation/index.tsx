import React from 'react'
import { withRouter, RouteComponentProps } from 'react-router-dom'
import { ClassifiCation } from '@/types/RecommendedReading'
import style from './index.less'
import { useIntl, useSelector } from 'umi'
import { IRootState } from '@/types'
interface Prop {
    infor: ClassifiCation[]
}

let ClassifiCationx: React.FC<Prop & RouteComponentProps> = function (props) {
    const intl = useIntl();
    const { language } = useSelector((state: IRootState) => {
        return {
            language: state.language
        }
    })
    return (
        <div className={style.RecommendedReading}>
            <div className={style.RecommendedReading_top}> <span> <b>{intl.formatMessage({id:'menu.category'})}</b> </span> </div>
            <div>
                <ul className={style.content}>
                    {
                        props.infor.map((item, index) => {
                            return <li key={index} className={style.content_li}
                                onClick={() => {
                                    props.history.push(`category/${item.value}`)
                                }}>
                                <span> {item.label} </span>
                                <span>{intl.formatMessage({id:'menu.total'})} {item.articleCount}{language.locale=='en-US'?intl.formatMessage({id:'menu.count'}):'篇文章'} </span>
                            </li>
                        })
                    }
                </ul>
            </div>

        </div>
    )
}

export default withRouter(ClassifiCationx)
