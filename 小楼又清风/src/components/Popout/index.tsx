//弹框
import { Form, Input, Button, Modal } from 'antd'
import { useIntl } from 'umi';
interface Props {
    isModalVisible: boolean
    handleCancel(): void
    onFinish(values: any): void
    onFinishFailed(errorInfo: any): void
}
const Popout: React.FC<Props> = (props) => {
    const intl = useIntl();
    const { isModalVisible, handleCancel, onFinish, onFinishFailed } = props
    return <Modal
        title={intl.formatMessage({id:'menu.psyi'})}
        visible={isModalVisible}
        footer={''}
        onCancel={handleCancel}
    >
        <Form
            name="basic"
            initialValues={{ remember: true }}
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
            labelAlign={"left"}
            labelCol={{ span: 4 }}

        >
            <Form.Item
                label={intl.formatMessage({id:'menu.username'})}
                name="name"
                rules={[{ required: true, message: '请输入您的称呼!' }]}

            >
                <Input />
            </Form.Item>

            <Form.Item
                label={intl.formatMessage({id:'menu.email'})}
                name="email"
                rules={[{ required: true, message: '输入合法邮箱地址，以便在收到回复时邮件通知!' }, {
                    type: 'email',
                    message: '请输入正确邮箱!',
                },]}

            >
                <Input />
            </Form.Item>
            <Form.Item wrapperCol={{ offset: 17, span: 15 }}>
                <Button 
                style={{ width: "64.16px",textAlign:"center" }}
                onClick={handleCancel}>{intl.formatMessage({id:'menu.cancel'})}</Button>
                <Button type="primary" danger htmlType="submit"
                    style={{ margin: "0 0 0 9px" }}>{intl.formatMessage({id:'menu.save'})}</Button>
            </Form.Item>
        </Form>
    </Modal>
}
export default Popout