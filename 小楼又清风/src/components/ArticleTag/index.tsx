import React from 'react'
import { TagItem } from '@/types'
import style from './index.less'
import {RouteComponentProps,withRouter} from 'react-router-dom'
import { useIntl } from 'umi'

interface Prop extends RouteComponentProps {
    TagData: TagItem[]
}

let ArticleTag: React.FC<Prop> = function (props) {
    const intl = useIntl();
    return (
        <div className={style.ArticleTag}>
            <div className={style.ArticleTag_top}> <span> <b>{intl.formatMessage({id:'menu.tags'})}</b> </span> </div>
            <div className={style.content}>
                <ul>
                    {
                        props.TagData.map(item => {
                            return <li key={item.id} onClick={()=> {
                                props.history.push(`/tag/${item.label}`)
                            }}>
                                <span>{item.label} [{item.articleCount}]</span>
                            </li>
                        })
                    }
                </ul>
            </div>

        </div>
    )
}

export default withRouter(ArticleTag)
