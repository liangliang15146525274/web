export interface ModelsItem {
  id: string;
  name: string;
  email: string;
  content: string;
  html?: string;
  pass: boolean;
  userAgent: string;
  hostId: string;
  url: string;
  parentCommentId?: any;
  replyUserName?: any;
  replyUserEmail?: any;
  createAt: string;
  updateAt: string;
  children: ChildItem[];
  flag?: boolean;
}

export interface ChildItem {
  id: string;
  name: string;
  email: string;
  content: string;
  html: string;
  pass: boolean;
  userAgent: string;
  hostId: string;
  url: string;
  parentCommentId: string;
  replyUserName: string;
  replyUserEmail: string;
  createAt: string;
  updateAt: string;
  flag?: boolean;
}
export interface IViewItem {
  id: string;
  cover: string;
  name: string;
  path: string;
  order: number;
  content: string;
  html: string;
  toc: string;
  status: string;
  publishAt: string;
  views: number;
  createAt: string;
  updateAt: string;
}
export interface IGetAbout {
  id: string;
  cover: string;
  name: string;
  path: string;
  order: number;
  content: string;
  html: string;
  toc: string;
  status: string;
  publishAt: string;
  views: number;
  createAt: string;
  updateAt: string;
}
export interface IGetMsgboard {
  id: string;
  cover?: any;
  name: string;
  path: string;
  order: number;
  content: string;
  html: string;
  toc: string;
  status: string;
  publishAt: string;
  views: number;
  createAt: string;
  updateAt: string;
}