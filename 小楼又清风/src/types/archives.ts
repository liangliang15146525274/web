export interface IArchivesItem {
    '2020'?: _2020;
    '2021'?: _2021;
}

export interface _2021 {
    July: July2[];
    May: June[];
    April: June[];
    March: June[];
    February: June[];
}

export interface July2 {
    id: string;
    title: string;
    cover?: any
    summary?: any;
    content: string;
    html: string;
    toc: string;
    status: string;
    views: number;
    likes: number;
    isRecommended: boolean;
    needPassword: boolean;
    isCommentable: boolean;
    publishAt: string;
    createAt: string;
    updateAt: string;
}

export interface _2020 {
    July: July[];
    June: June[];
    May: May[];
    April: June[];
    March: June[];
    February: June[];
}

export interface May {
    id: string;
    title: string;
    cover: string;
    summary?: any;
    content: string;
    html: string;
    toc: string;
    status: string;
    views: number;
    likes: number;
    isRecommended: boolean;
    needPassword: boolean;
    isCommentable: boolean;
    publishAt: string;
    createAt: string;
    updateAt: string;
}

export interface June {
    id: string;
    title: string;
    cover: string;
    summary: string;
    content: string;
    html: string;
    toc: string;
    status: string;
    views: number;
    likes: number;
    isRecommended: boolean;
    needPassword: boolean;
    isCommentable: boolean;
    publishAt: string;
    createAt: string;
    updateAt: string;
}

export interface July {
    id: string;
    title: string;
    cover: string;
    summary?: string;
    content: string;
    html: string;
    toc: string;
    status: string;
    views: number;
    likes: number;
    isRecommended: boolean;
    needPassword: boolean;
    isCommentable: boolean;
    publishAt: string;
    createAt: string;
    updateAt: string;
}


export interface IArticleDetail {
    id: string;
    title: string;
    cover: string;
    summary: string;
    content: string;
    html: string;
    toc: string;
    status: string;
    views: number;
    likes: number;
    isRecommended: boolean;
    needPassword: boolean;
    isCommentable: boolean;
    publishAt: string;
    createAt: string;
    updateAt: string;
    password?: any;
}