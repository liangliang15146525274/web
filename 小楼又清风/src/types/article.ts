//推荐文章
export interface IArticleItem {
  id: string;
  title: string;
  cover?: string;
  summary?: string;
  content: string;
  html: string;
  toc: string;
  status: string;
  views: number;
  likes: number;
  isRecommended: boolean;
  needPassword: boolean;
  isCommentable: boolean;
  publishAt: string;
  createAt: string;
  updateAt: string;
  category?: Category;
  tags: Category[];
}

interface Category {
  id: string;
  label: string;
  value: string;
  createAt: string;
  updateAt: string;
}

//分类标题
export interface HTileItem {
  id: string;
  label: string;
  value: string;
  createAt: string;
  updateAt: string;
  articleCount: number;
}

//列表
export interface ArticleItem {
  id: string;
  title: string;
  cover?: string;
  summary?: string;
  content: string;
  html: string;
  toc: string;
  status: string;
  views: number;
  likes: number;
  isRecommended: boolean;
  needPassword: boolean;
  isCommentable: boolean;
  publishAt: string;
  createAt: string;
  updateAt: string;
  tags: Tag[];
  category?: Tag;
}

interface Tag {
  id: string;
  label: string;
  value: string;
  createAt: string;
  updateAt: string;
}

//文章分类
export interface TagItem {
  id: string;
  label: string;
  value: string;
  createAt: string;
  updateAt: string;
  articleCount: number;
}
